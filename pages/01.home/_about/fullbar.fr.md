---
title: Disroot is
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot est une plateforme qui fournit des services en ligne basés sur les principes de liberté, de respect de la vie privée, de fédération et de décentralisation.
**Pas de suivi, pas de publicité, pas de profilage, pas d'exploitation de données!**
