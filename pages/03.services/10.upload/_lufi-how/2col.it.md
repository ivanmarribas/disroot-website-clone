---
title: 'Lufi come funziona'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Come funziona?

Trascinare e rilasciare i file nell'area appropriata o utilizzare il modo tradizionale per sfogliare e selezionare i documenti e i file verranno sbriciolati, crittografati e inviati al server. Si otterranno due link per file: un link per il download, che si dà alle persone con cui si desidera condividere il file, e un link di eliminazione, che consente di eliminare il file quando si desidera.
Puoi vedere l'elenco dei tuoi file cliccando sul link "I miei file" in alto a destra di questa pagina.
Non è necessario registrarsi per caricare i file.

---

![](en/lufi-drop.png?lightbox=1024)
