---
title: 'Rule'
bgcolor: '#8EB726'
fontcolor: '#FFF'
text_align: center
---

## Disroot does not record your IP address!
*Use the button at the top of the page to install the Disroot search plugin on your Firefox.*
