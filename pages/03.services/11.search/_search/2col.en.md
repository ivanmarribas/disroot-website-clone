---
title: Search
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Go Searx'ing</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Install browser plugin</a>
<a class="button button1" href="http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion">Tor</a>

---

![](searx_logo.png)

# Anonymous multi search engine platform

Disroot's Search is a search engine like Google, DuckDuckGo, Qwant, powered by **Searx**. What makes it unique from others is that it is a metasearch engine, aggregating the results of other search engines while not storing information about its users.

You don't need any account on Disroot to use this service.

Disroot Search: [https://search.disroot.org](https://search.disroot.org)

Source code: [https://github.com/asciimoo/searx](https://github.com/asciimoo/searx)
