---
title: 'Spazio di archiviazione extra'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Spazio di archiviazione extra

Se i 2GB di spazio gratuiti non sono sufficienti, puoi estendere il tuo spazio di archiviazione nel cloud.

Ecco i prezzi **all'anno, spese di pagamento incluse**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |

<br>
Le transazioni all'interno dell'UE sono soggette a un'IVA aggiuntiva (imposta sul valore aggiunto) del 21%.

---

<br><br>

<a class="button button1" href="https://disroot.org/it/forms/extra-storage-space">Richiedi spazio extra</a>
