---
title: 'Synchronisation de Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: Bureau
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: 'Téléphone'
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Navigateur
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Accès direct depuis le navigateur'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Synchronisation de Nextcloud

Accédez à vos données sur n'importe quelle plate-forme. Utilisez les clients **Android** ou **iOS** pour travailler avec vos fichiers en déplacement, ou synchronisez vos dossiers favoris en toute transparence entre votre ordinateur de bureau et vos ordinateurs portables.

---

![](en/nextcloud-files.png?lightbox=1024)
