---
title: 'Pads Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Pads collaboratifs


Gestion d'**Etherpad** et d'**EtherCalc** depuis **Nextcloud**.


**Attention: Les pads et les feuilles de calcul sont gérés par un autre service et ne sont pas stockées dans votre cloud. De plus les pads peuvent être accédées théoriquement par tout le monde. Il suffit d'avoir l'url d'accès.**

---

![](en/OCownpad.png?lightbox=1024)
