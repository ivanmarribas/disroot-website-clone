---
title: 'Nextcloud Pads'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Pad collaborativi

**Etherpad** e **EtherCalc** possono essere gestiti all'interno di **Nextcloud**.

**Attenzione, tutti i tuoi pad sono salvati a tempo indeterminato sui server**.

---

![](en/OCownpad.png?lightbox=1024)
