---
title: Nextcloud
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Registrati su **Disroot**</a>
<a class="button button1" href="https://cloud.disroot.org/">Accedi</a>
<a class="button button1" href="http://3rhtbo7bb3o5qvx2iyirppxxtpwnlxkavdyjdckmuzh2uohibignufqd.onion">Tor</a>

---
![](Nextcloud_Logo.png)

**Nextcloud** è un software open source. Con **Nextcloud** è possibilie sincronizzare e condividere i propri documenti, calendari, contatti e altro ancora.

Permette di condividere in modo sicuro utilizzando standard compatibili con qualsiasi sistema operativo.

Tutti i dati salvati su **Nextcloud** sono criptati. Questo significa che nessuno è in grado di vedere il contenuto dei tuoi documenti senza il tuo permesso. Nemmeno gli amministrazione del sistema hanno accesso a queste informazioni.

Disroot Cloud: [https://cloud.disroot.org](https://cloud.disroot.org)

Homepage progetto: [https://nextcloud.com](https://nextcloud.com)

Codice sorgente: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
