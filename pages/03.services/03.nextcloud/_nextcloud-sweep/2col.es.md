---
title: 'Nextcloud Keep or Sweep'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-sweep.png?lightbox=1024)

---

## Keep or Sweep (Mantener o barrer)

Esta pequeña aplicación te ayuda a quitar el desorden recordándote borrar archivos que quizás hayas olvidado.
