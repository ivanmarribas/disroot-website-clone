---
title: 'Notes'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: Notes
        logo: en/notes_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.owncloud.notes/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-notes.png?lightbox=1024)

---

## Notes

Create notes, share and sync them with all your devices.
