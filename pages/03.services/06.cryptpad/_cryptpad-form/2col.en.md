---
title: 'Form'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](form.png)

## End to End Encrypted Form
**Form** offer you to create and share totally end-to-end encrypted forms.


---
![](presentation.png)

## Encrypted Presentations on the fly
Create end-to-end encrypted presentations with easy editor and together with your friends or team members. Your finished presentation can be "played" directly from cryptpad.
