---
title: Clientes de Correo'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Utiliza tu cliente de correo preferido
Hay un montón de opciones en clientes de Correo para el escritorio y dispositivos móviles. Elige el que más te guste.<br>

Mira nuestra [página de manuales](https://howto.disroot.org/es/tutorials/email/clients) información en detalle sobre cómo configurar tus clientes favoritos.
<br>
*Verifica las configuraciones en la parte superior de esta página para configurar tu cuenta de correo de* **Disroot** *en tu dispositivo. Estos ajustes son información estándar que le dicen a tu cliente de correo cómo conectar con el servidor de correo de* **Disroot**.
