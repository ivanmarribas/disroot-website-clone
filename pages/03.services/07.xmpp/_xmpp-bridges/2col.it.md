---
title: 'Xmpp Bridges'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations2.png)

---

## Gateway, ponti e trasporti

XMPP consente vari modi per connettersi a diversi protocolli di chat. 

### Collegati a una stampa IRC
Puoi connetterti a qualsiasi stanza IRC ospitata su qualsiasi server IRC tramite il nostro gateway IRC Biboumi: 
<ul class=disc>
<li>Per collegarsi ad una stanza IRC: <b><i>#room%irc.domain.tld@irc.disroot.org</i></b></li>
<li>Per aggiungere un contatto IRC: <b><i>contactname%irc.domain.tld@irc.disroot.org</i></b></li>
</ul>

Dove ***#room*** è la stanza IRC a cui vuoi collegarti e ***irc.domain.tld*** è l'indirizzo del server IRC. Assicurati di lasciare ***@irc.disroot.org*** così com'è, perché è l'indirizzo del gateway IRC. 

### Collegati a una stanza Matrix
Puoi connetterti a qualsiasi stanza Matrix ospitata su qualsiasi server Matrix tramite il bridge Matrix.org:
<ul class=disc>
<li>Per collegarsi a una stanza Matrix: <b><i>#room#matrix.domain.ldt@matrix.org</i></b></li>
</ul>

Dove ***#room*** è la stanza Matrix a cui vuoi collegarti e **matrix.domain.tld*** è l'indirizzo del server Matrix. Assicurati di lasciare ***@matrix.org*** così com'è, perché è l'indirizzo del bridge Matrix. 

### In divenire...
In futuro stiamo pianificando di gestire il nostro ponte Matrix. Speriamo anche di fornire un ponte Telegram e una serie di altri ponti e trasporti. 
