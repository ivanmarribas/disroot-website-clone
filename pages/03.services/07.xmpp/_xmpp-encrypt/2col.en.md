---
title: 'Xmpp Encrypt'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Encrypt everything!

By using OMEMO (GPG or OTR) End-to-End encryption methods on the client side (i.e. on your end), your conversations will reach the recipient of the message without being intercepted by anyone (not even administrators).

---

![](omemo_logo.png)
