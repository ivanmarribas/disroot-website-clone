---
title: 'Akkoma Funktionalitäten'
wider_column: left
---

## Föderation
Sie können Nachrichten mit Personen auf beliebigen ActivityPub- oder OStatus-Servern austauschen, wie GNU Social, Friendica, Hubzilla und Mastodon.

## Schreibe, was dir in den Sinn kommt!
Mit Akkoma bist du nicht auf 150 Zeichen beschränkt! Du kannst Links, Bilder, Umfragen, etc. einfügen.

## Benachrichtigungen
Erhalte Benachrichtigungen, wenn dich jemand als Kontakt hinzufügt, wenn du in einem Beitrag erwähnt wirst oder wenn einer deiner Beiträge von jemandem geteilt wird.

## Chat
Sende Nachrichten an andere Mitglieder von Disroot's Akkoma mit einem Real Time Chat.

---
<b>

![](en/Akkoma_home.png?lightbox=1024)
