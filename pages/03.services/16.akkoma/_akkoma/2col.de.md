---
title: Akkoma
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://fe.disroot.org/">Anmelden</a>

---

![akkoma_logo](akkoma.png?resize=100,100)


## Akkoma

**Akkoma** ist eine Microblogging-Server-Software, die mit anderen Servern, die ActivityPub unterstützen, wie Friendica, GNU Social, Hubzilla, Mastodon, Misskey, Peertube und Pixelfed föderieren (= Nachrichten austauschen) kann.

Disroot Akkoma: [https://fe.disroot.org](https://fe.disroot.org)

Project homepage: [https://akkoma.social/](https://akkoma.social/)

Source code: [https://akkoma.dev/AkkomaGang/akkoma](https://akkoma.dev/AkkomaGang/akkoma)
