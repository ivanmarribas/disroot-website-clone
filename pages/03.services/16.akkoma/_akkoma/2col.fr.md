---
title: Akkoma
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://fe.disroot.org/">Se connecter</a>

---

![akkoma_logo](akkoma.png?resize=100,100)


## Akkoma

**Akkoma** est un logiciel serveur de microblogging qui peut fédérer (= échanger des messages, des informations) avec d'autres serveurs qui supportent ActivityPub, comme Friendica, GNU Social, Hubzilla, Mastodon, Misskey, Peertube et Pixelfed.

Disroot Akkoma: [https://fe.disroot.org](https://fe.disroot.org)

Page d'accueil du projet: [https://akkoma.social/](https://akkoma.social/)

Code sourceode: [https://akkoma.dev/AkkomaGang/akkoma](https://akkoma.dev/AkkomaGang/akkoma)
