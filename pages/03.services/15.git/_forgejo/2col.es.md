---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Sign up</a>
<a class="button button1" href="https://git.disroot.org/">Log in</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Git de Disroot** está desarrollado por **Forgejo**. **Forgejo** es una solución impulsada por la comunidad, poderosa, liviana y fácil de usar, para hospedar código y proyectos colaborativos. Está construida sobre la tecnología [GIT](https://es.wikipedia.org/wiki/Git), que actualmente es el más moderno y ampliamente utilizado sistema de control de versiones en el mundo.

Git de Disroot: [https://git.disroot.org](https://git.disroot.org)

Página del proyecto: [https://forgejo.org/](https://forgejo.org/)

Código fuente: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

Necesitas crear una cuenta aparte en [git.disroot.org](https://git.disroot.org/) para utilizar el servicio. Las credenciales de **Disroot** no son compatibles.
