---
title: Audio
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://mumble.disroot.org/">Verbinden</a>
<a class="button button1" href="http://vd2k6x3cvqmm7pt2m76jpcyq7girnd3owykllkpu73rxzqv2cbs5diad.onion">Tor</a>

---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

Disroots Audio-Anwendung wird mit *Mumble* realisiert. Mumble ist eine kostenlose, quelloffene Voice-Chat-Anwendung mit niedriger Latenz und hoher Sprachqualität. Ursprünglich war sie für Gamer gedacht, aber sie kann auch zur Organisation von Audio-Meetings, Konferenzen usw. verwendet werden.

**HINWEIS!**

Du brauchst kein Konto, um Mumble zu benutzen. Aber Du hast mehr Nutzerrechte, wenn Du Deinen Benutzernamen registrierst.

Disroot Mumble: [mumble.disroot.org](https://mumble.disroot.org)

Projekt-Homepage: [https://www.mumble.info](https://www.mumble.info)

Quellcode: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
