---
title: 'Mumble Features'
wider_column: left
---

## Facile da usare

Scarica l'app Mumble disponibile sul tuo dispositivo (desktop, Android o iOS), connettiti a [mumble.disroot.org](https://mumble.disroot.org/) e potrai già iniziare a creare un canale audio e parlare con le persone!

## Una buona qualità audio

Bassa latenza e soppressione del rumore, quindi è ottimo per parlare. Puoi avere circa 100 partecipanti simultanei! 

## Privato e sicuro

Le comunicazioni sono crittografate e l'autenticazione è tramite chiave pubblica/privata per impostazione predefinita. Se utilizzi il software Desktop Mumble, puoi concedere autorizzazioni molto specifiche ai tuoi canali, gruppi e utenti. 

## App per mobile

Gestisci il tuo progetto in movimento con l'app mobile Android (disponibile su F-Droid) o l'app iOS. 

---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
