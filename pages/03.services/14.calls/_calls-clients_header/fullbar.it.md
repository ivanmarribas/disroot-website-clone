---
title: 'Jitsi Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usa la tua applicazione preferita
Ci sono client desktop/web/mobile tra cui scegliere. Scegli quello che ti piace di più. 
