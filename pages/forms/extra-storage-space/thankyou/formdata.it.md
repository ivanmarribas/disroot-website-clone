---
title: 'Richiesta di archiviazione extra'
cache_enable: false
process:
    twig: true
---

<br><br> **Abbiamo ricevuto la vostra richiesta. <br><br>Riceverai una conferma via e-mail con il tuo riferimento di fatturazione. <br> Una volta ricevuto il tuo pagamento ti assegneremo il deposito extra. <br><br> Grazie per il sostegno a Disroot!**
<br>
<hr>
<br>
**Ecco una sintesi della richiesta ricevuta:**
