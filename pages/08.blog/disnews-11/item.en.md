---
title: 'DisNews #11 - Fighting spam on Halloween'
date: '17-11-2023'
media_order: our_tears_are_the_same.jpg
taxonomy:
  category: news
  tag: [disroot, news, spam, cryptpad, libretranslate, lacre, chatcontrol]
body_classes: 'single single-post'

---
Hi there Disrooters.

Hallo there. Missed us? Well, silent but not lazy. Last time, we wrote that the world was going crazier each month. Sadly, seems we couldn't be more right about it. While waiting for imminent UFO invasion (what else is left to surprise us right?), here is some highlights on what we've been busy with.


# New perk: Extra storage on Cryptpad
We have been hosting Cryptpad for several years now, and we're really happy about it. It's a great set of collaboration services for those who value privacy as all data is end to end encrypted.

Recently, some Disrooters contacted us to see if they could have extra storage on Cryptpad, like we do for mail and cloud. So first we extended the **default free storage from 250MB to 500MB for everyone \o/**.

We've also decided to allow user to allocate the extra storage they pay for as they wish between cloud, mail and now also Cryptpad. For example, if a Disrooter donates for 10GB storage, this user could decide to have 6GB for cloud, 2GB for mail and 2GB for Cryptpad. These extra gigabytes will be added on top of the default storage space given on account sign-up (so in that previous example the user would have 2,5GB of extra storage on Cryptpad).

Check our [Perks page](https://disroot.org/perks) to see the extra storage prices.


# Forgejo: Support incoming email
Forgejo is the software powering our [git instance](https://git.disroot.org). Last month we have added new feature called "incoming email". This feature allows for several actions that can be executed via email such us replying to issues or mentions. Specially useful if you want to stay active on your project while on the go.


# New services
For Halloween :), we've deployed two new services.

First, we have replaced ConverseJS with **Movim** as a xmpp chat service. It's far more user friendly which is something many of you were asking for. Apart from great interface, it provides also a form of federated social network features such as communities and blogging. All this federated with other Movim/XMPP nodes and in the future part of fediverse which is powering our [akkoma instance](https://fe.disroot.org) instance and thousands of others in the network. We would like to thank a lot of all people involved in [ConverseJS]( https://conversejs.org/) project for supporting and developing this webchat solution. We would like to send our love and greeting to [jcbrand](https://mastodon.xyz/@jcbrand) for all his work and support. Thanks for all the fish!

Second, service is a translation service you can find under [translate.disroot.org](https://translate.disroot.org). It is powered by [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate) software. Our instance is still fresh and possibly unstable so if you notice anything worth feedback, please feel free to do so. It will help us get things stable and awesome!


# Fight against spam
As usual, the never ending war on spam continues. We're trying our best to combat this plague. This time we have tackle mostly the outgoing SPAM from Disroot itself. We are dealing with quite an amount of abusers creating Disroot accounts to send out spam, phishing and other abuse. This harms our reputation as a domain which results in emails sent from Disroot in general being viewed by others as spam. We did add some measures to combat this activity without harming users privacy and we do see some progress on the field. It does require consistency and adding yet another task to daily routine but we are sure it will yield results. Hopefully Disroot will become too much of a hassle for abusive scum so that they will loose interest and move away to look for easier prey.

Additionally, we have enabled a feature on [webmail](https://webmail.disroot.org) allowing users to mark emails as spam. Those marked emails are then forwarded automatically to us, and we can use them to further teach spam filters on our servers. If you do get some spam, please mark it as such. If you get a lot of it, move them to your Junk folder and drop us a line at support@disroot.org. We will then take care of the Junk mails for you and feed it to our ever hungry spam filtering system. If you use native client forwarding your spam to spam.report@disroot.org and your ham (false positive) ham.report@disroot.org will do the trick also.

# New EU regulations to destroy user privacy
Yet again, our privacy and safety in the internet is challenged. It seems that governments cannot stand the fact internet is our last free bastion to exchange information and ideas and are doing what they can to turn that medium into controlled one, just like radio, newspapers or television.

First they are coming for your correspondence. ChatControl legislation as a result of war against child pornography and grooming is planning to destroy any end-to-end encryption messaging as the authorities require service provider to snoop on all messages to filter out the suspicious ones. It destroys fundamental rights for your privacy of correspondence while totally missing the goal, as child abusers will continue to find the ways to distribute their content. It is much failed approach as solving homelessness with a legislation or banning kitchen knifes as they could be used to harm or kill others.

Secondly, EU behind closed doors is trying to push legislation that will force all web-browsers (and not only) to implement certificate authorities and cryptographic keys selected by EU governments. This essentially would allow government (or anyone else for that matter) who's in possession of such certificates to intercept encrypted data you exchange with the website and view it in plain in real time. For example, emails you send, posts, you upload, images you view, things you download or upload, people you communicate with. Essentially everything you do on the internet could be viewed in real time by the state. This is no joke, nor it's a far fetched conspiracy theory. This is our reality!

The fight for our freedoms continues as long as the EU tries to implement regulations straight from totalitarian nightmares. Check the links below and get involved. It is about your privacy and freedoms after all:

  - [https://stopchatcontrol.eu/](https://stopchatcontrol.eu/)

  - [https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years](https://www.eff.org/deeplinks/2023/11/article-45-will-roll-back-web-security-12-years)

  - [https://last-chance-for-eidas.org/](https://last-chance-for-eidas.org/)

  Also check out a great [video](https://yewtu.be/watch?v=TwGRS8IldH0) from Nicco Loves Linux about this.


# What is coming up?
Next two months we want to focus on proper end of the year cleaning. We will try to cleanup, answer and finalize everything that is either urgent or easy to close on our issue board, support ticket queue and all started but unfinished. We will try to answer all old pending tickets too. We hope to welcome new year with nice clean slate, as little backlog as possible. Keep your fingers crossed. 

We tried to resurrect the idea of XMPP custom domain but we have hit a concrete wall in the process and have not managed to get it up and running. The good thing though is that we are working on it finally, and we will try to find a solution to the issue we currently have. Hopefully it's a solvable matter. If you are interested in following the development of this feature or want to actively participate, check [this issue page](https://git.disroot.org/Disroot/Disroot-Project/issues/319)

Before the end of the year we hope to finally test Lacre (our approach to end-to-end mailbox encryption). @pfm has done quite some progress and we think we are ready, so stay tuned for more details coming really soon.


Stay cool!

-Disroot Core Team
