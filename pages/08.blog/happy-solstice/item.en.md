---
title: 'Happy Solstice'
media_order: sunrise-over-the-earth.jpg
published: true
date: '22-12-2018 22:32'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---
First of all we would like to wish you all a late Happy Solstice! For folks on the same hemisphere as us it means days are finally getting longer while for folks living "up side down" the opposite unfortunately. We hope you are all prepping up for your new years resolutions and will have a blast on the last days of the year. We sure we will.

So, you haven't heard from us in a while. Despite our intentions to post every two weeks, too much work got in our way. Here is some of the things you've missed if you haven't followed us closely (via chat or social media posts).


## 1. New CPUs - More horse power.
Our main server got new and shiny CPUs! Thanks to this upgrade we've gained much needed performance boost. Although we now enjoy some of the load taken off by few extra cores, we have also reached the maximum processor power possible for the current server. This means that if we need to upgrade it in the future, we will have to invest in new hardware - which is something we have to start thinking about in the next year.

## 2. Taiga 4.0
Taiga got a new major version packed with number new features. Some of the highlights include:
 - attach issues to sprint
 - link user-stories to epics directly upon creation
 - redesign lightbox - now more streamlined and improved editor lets you create new user stories with all needed details faster
 - due dates - although not very agile now you can set due dates on your tickets
 - assign tickets to multiple members of the project
 - Notifications - new feature allowing you to see whats new on all your projects in one glance via top bar notification icon

for more details check taiga's [blog](https://blog.taiga.io/) packed with news about this wonderful project management platform.

## 3. Nextcloud 14 update.
This time update has come very late. In fact nextcloud 15 has just been released and we are preparing for update in coming days. This delayed update was due to some issues we wanted to fix prior. This took much more time then expected - in fact we were still implementing a solution to one of the issues just an hour ago! - but we can already see big improvements in the performance in the last weeks. We are awaiting the new version update which will bring more noticeable visual changes. We expect it to land before the new year.

## 4. New registration process.
It's been few good weeks we are running the new system and we are enjoying it a lot. We did some improvements along the way and changed few requirements, and we are very amused by people's stories. Keep them interesting to make this manual process as fun as it can get. Currently although not completely water tight, we are very good in repealing all kind of abusers that used to swarm Disroot before.

## 5. Disroot app rocks.
We cannot express our appreciation and gratitude enough. Massimiliano, the author of the Disroot app, has done some incredible work. In the new update (hitting f-droid in matter of days) the app also provides support for https://state.disroot.org where your phone will receive a notification if there is a new issue, planned maintenance work, downtime or outage. Thanks a lot man! You Rock!!1one
If you don't have the app yet, quick, install it via [web](https://f-droid.org/en/packages/org.disroot.disrootapp/) or directly from the best android app store there is (if you haven't install it yet, do it now!): [F-DROID](https://f-droid.org)

Overall we've been working on various improvements of our infrastructure the last weeks. In small steps we are getting closer and closer to fully automated Disroot setup. Before the end of the year, while trying to enjoy the festive moments of the last days of 2018, we want to work on some last awaiting issues that need attention, finalize yearly report (which is looking awesome), and finally update Hubzilla (we know this is taking forever, and we are sorry for that, but we are really working on it).

Stay tuned for more frequent posts (one of our new years resolution).
