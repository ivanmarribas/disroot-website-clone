---
title: 'First half of 2020'
media_order: sunflower.jpg
published: true
date: '07-08-2020 13:10'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

# First half of 2020

Hi there,

The first half of 2020 is almost behind us, and it feels like someone packed in at least 5 years worth of events into it. Seems like the few things left for the second half is alien invasion, sun explosion and the discovery of time travel or teleportation. Looking at recent world wide developments, none of us would be particularly surprised if one of the above things would take place. For many around the world, life has been turned up side down. Since the project just turned 5 this month, perhaps it is time for a quick summary of the last months at Disroot and for sharing some ideas for the upcoming half a year.

## Mailbox encryption

As you could read in previous posts, we set a goal of bringing mailbox encryption to Disroot this year. We decided to explore two possibilities: server side encryption and end-to-end encryption of mailboxes.

**Server side** encryption is a form of encryption where both public and private keys are kept on the server. Very similar to how files are encrypted on your Disroot cloud (Nextcloud). There is no need to run any additional, specialized software on users's devices - this means users can use the mail clients (apps) of their choice - thus IMAP/POP3 compatibility is kept. However, there are few down sides to server side encryption. One is, of course, that the encryption keys are stored on the server, meaning that by knowing the password of the user (through Man in the Middle Attack or some form of Brute Force Attack to name a couple of examples) it is possible to decrypt emails. Additionally changing / resetting passwords posses another issue where just like with the cloud, users need to either request removal of the encryption keys (in case of password reset) and will no longer be able to decrypt previous emails, or needs to update their keys with new password (given old password is known to user). Since this seem to be quite of a headache without much of gain (with a bit of an effort, someone gaining control of the server could decrypt mailboxes), we also looked at end-to-end encryption possibilities.

**End-to-end** encryption is one where the private key used to encrypt your emails is stored on your device. In that way even with access to the server it is impossible to decrypt emails stored in the mailbox without getting hold of your private key, which can only happen if access to your device is gained. Some providers out there that provide some sort of end-to-end encryption, create a form of walled garden or closed ecosystem where you need to use only a mail client provided by the platform (no choice in clients), are incompatible with other providers (own in-house developed encryption), do not allow you to control the encryption keys (you don't really own the keys as you cannot easily extract them, generate them on a separate machines, back them up, all in the name of making it "easy" for users etc), and some of them even keep your private key on the server (making it essentially server side). For us it is important that end-to-end solution would give disrooters freedom of choice as well as provide full control over the private keys, be - as much as possible - on pair with standards (do not reinvent the wheel) and be fully open source so that other providers within the Librehost realm and beyond could adopt it and create interoperable end-to-end email storage and transmit encryption.

After looking around and trying different solutions, we have stumbled upon a software called *"GPG Mail Gate"*. Although seems abandoned for years, it looks like it solves all issues in one go:

* GPG - uses GnuPG standard for email encryption. This means it will work not only within Disroot but could be used to encrypt emails to others and others could easily encrypt to you.
* Public key upload - Interface that allows you to upload your public key, which means the server will automatically encrypt all of your incoming emails with your public key while your private key stays secret on your device and Disroot server never sees nor has access to or helps generate one. Additionally with a key server in place, your public key could be used by anyone out there to automatically encrypt emails to you. No need to share keys manually
* Encrypting emails you send out - even if you send in plaintext, all emails sent by you would be stored end-to-end encrypted on the server
* Easy setup - does what it needs to do and does not require rocket scientists to operate

### *So,* *when would we be running it?*

Since the software is quite outdated, we would like to first find new maintainer, re-write some parts of it, and create better mechanism, embedded in the webmail for uploading keys. Additionally we would like to make it Autocrypt compliant to make it that much more robust.

#### *Want to help?*

We are looking for developers who are willing to take on the maintenance of the project, rewrite it with python3 (or any other choice of language), and redo the key upload web service. We are trying to secure some funding for it as well so might pay a bit more than a beer (maybe a crate of beer) to help fund the work that has potential to benefit bigger network of email providers. If you are willing to help out, please get in touch with us.
You can find the current code on our [git](https://git.disroot.org/Disroot/gpg-mailgate)

## Themes

As a part of the work we have started last year of providing more unified experience, Meaz have started working on themes for some of the services. At this moment we have "disrooted" Hubzilla, Lufi, Nextcloud, Roundcube *(coming soon...)*, and we have Searx, Privatebin and ConverseJS almost there too. This is just the initial work but it opens possibilities for us not to only focus on color pallets and small design elements but also start working on general app design which will improve User eXperience. So far we are having fun doing it and we are excited to see where we can take this in the future.

## Volounteer fee

Disroot has come a long way in our financial strive for independence and sustainability. Starting from the time we paid all the costs from our own pockets, through the moment when costs were finally paid by donations and to the moment when we started donating back to FLOSS software.

We decided it is time to set a new milestone on that journey. Disroot is our precious gem. We spend more time on it than we do on our day jobs (don't tell this to our bosses). While the platform grows so does the time needed for it. Our ultimate goal is to reach financial independence and focus on Disroot as our main occupation while sticking to our initial ideas behind our "business model" and not "sell out". Current situation is not sustainable and our private/family life suffers from it as the day has only 24 hours and you still need to fit enough time for sleeping and stuff (something we lack as well).

We are aware of the fact that our chosen path to achieve this very ambitious goal is not easy. We do believe that a social approach on economy - letting people decide how much they can and are willing to financially contribute to the project - is possible and can generate sustainable balance between those who can afford paying, those who are not able to pay for the services, and those who maintain the platform and get paid in return. We believe this is the right way to go further, but the road leading to that ultimate goal is far and will take long. So, we decided to take it one step at a time.

Dutch legislation allows foundations such as ours (in our case a non-profit one) to pay a small fee for volunteer work. It cannot be more than 170 euro a month or more than 1700 euro a year in total (= aprox. 140 euros a month). Seeing our financial situation allows for such payments, we decided to start a Disroot Core Team volunteer fee program. The Team is currently made of 4 people.

In addition to the costs and FLOSS donations, this year we also decided to set aside up to 400 euros per month for unexpected expenses as well as for future investments in hardware. Once the costs and expenses are covered, if there are at least 140 euros left, we could pay a volunteer fee to one of the Team members, if there are over 280 euros we could pay two volunteers and so on. As a safety net, if we fail to gather sufficient funds three months in a row, we will stop paying the volunteer/s.

We are glad to announce that we are already able to pay the fee to two of our volunteers 🎉. We will strive to keep them covered and to get others on board too. Once we get everyone onboard, there will be no force to stop us from another milestone of getting everyone on proper payroll.

We are counting on your help. It's important to realize that any amount helps. If everyone would pay 1 euro a month for the services Disroot has to offer, not only we would be able to pay all the costs, invest in better hardware, sponsor free and open source software development, but even salary to all the admins. So don't wait until you are able to pay lots of money to us, start small and lets grow together.

Think of us when you buy your favourite beverage each month in you local bar, cafe or at home and just 'buy' as a drink.
Check our [donation page](https://disroot.org/donate)

## 2019 yearly report

Now, this went totally south this year. For some reason writing yearly report took us half a year. We are not sure what exactly happened. We could blame Coronavirus or just generally 2020 as a whole, but that is just taking a shortcut. Most likely the amount of work that has piled up and the constant chase of priorities has caused the yearly report to be pushed into the backlog abyss. Well, finally we have managed it and there it is [link](https://disroot.org/en/annual_reports/AnnualReport2019.pdf)

We hope you like it and since it came out "slightly" too late it is a good moment to check the past year's highlights.

## What is planned for second half of the year

We want to focus our work on better User experience. Fede is working hard to update all the tutorials and howtos, and is producing new ones. We will continue working on themes and UX (use experience) improvements. But mainly we will be focusing on improving the email service. We want to launch better spam protection, new webmail, mailbox encryption and better handling of custom domain and alias requests.

So unless aliens invade our planet, Earth gets swallowed by a black hole, get hit by Nibiru or any other unexpected event comes our way this year, we are looking towards busy months ahead with lots of exciting work.
