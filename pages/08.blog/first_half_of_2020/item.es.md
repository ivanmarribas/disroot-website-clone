---
title: 'Primera mitad de 2020'
media_order: sunflower.jpg
published: true
date: '07-08-2020 13:10'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

# Primera mitad de 2020

¡Hola!

La primera mitad de 2020 está detrás nuestro y es como si alguien hubiera amontonado 5 años de acontecimientos en ella. Parece que la pocas cosas que quedan para la segunda mitad son una invasión alienígena, la explosión del sol y el descubrimiento de los viajes en el tiempo o la tele-transportación. Observando el desarrollo de los sucesos recientes a nivel mundial, ningunx de nosotrxs estaría particularmente sorprendidx si alguna de ellas sucediera. A muchxs en todo el mundo, la vida se les ha dado vuelta. Y ya que el proyecto acaba de cumplir 5 años, quizás sea el momento de hacer un rápido resumen de lo sucedido en **Disroot** en los últimos meses y compartir algunas ideas para el próximo medio año.

## Cifrado del buzón de correo

Como pudieron leer en posts anteriores, nos pusimos como meta llevar el cifrado del buzón a **Disroot** este año. Decidimos explorar dos posibilidades: cifrado del lado del servidor y cifrado de extremo a extremo de los buzones.

**El cifrado del lado del servidor** es una forma de cifrado en la que tanto la clave pública como la privada se mantienen en el servidor. Muy similar a la forma en que se cifran los archivos en la nube de **Disroot** (Nextcloud). No hay necesidad de ejecutar ningún software especializado adicional en los dispositivos de los usuarios y usuarias - esto significa que pueden utilizar los clientes de correo (apps) de su elección - por lo que se mantiene la compatibilidad con IMAP/POP3. Sin embargo, este método tiene algunos inconvenientes. Uno de ellos es, por supuesto, que las claves se almacenan en el servidor, lo que significa que conociendo la contraseña del usuario o la usuaria (a través de un Ataque de intermediario o de fuerza bruta, por nombrar un par de ejemplos) es posible descifrar los correos electrónicos. Además, el cambio o restablecimiento de las contraseñas plantea otro problema, ya que, al igual que con la nube, lxs usuarixs deben solicitar la eliminación de las claves de cifrado (en caso de restablecimiento de la contraseña) y por lo tanto ya no podrán descifrar los correos electrónicos anteriores, o deben actualizar sus claves con una nueva contraseña (en caso que el usuario o usuaria conozca la contraseña anterior). Dado que esto parece ser un gran dolor de cabeza sin mucho beneficio (con un poco de esfuerzo, alguien que obtenga el control del servidor podría descifrar los buzones de correo), también examinamos las posibilidades de cifrado de extremo a extremo.

Con el cifrado de **Extremo-a-extremo** la clave privada utilizada para cifrar tus correos se almacena en tu dispositivo. De esta manera, incluso con acceso al servidor, es imposible descifrar los emails almacenados sin conseguir tu clave privada, lo que solo puede suceder si se obtiene acceso a su dispositivo. Algunos proveedores que ofrecen algún tipo de cifrado de extremo-a-extremo, crean una especie de jardín vallado o ecosistema cerrado en el que necesitas utilizar un cliente de correo proporcionado por la plataforma (no puedes elegir el cliente), son incompatibles con otros proveedores (tienen un cifrado de desarrollo propio), no te permiten controlar las claves de cifrado y en verdad no son tuyas, ya que no se pueden extraer fácilmente, generarlas en máquinas separadas, hacer copias de seguridad y todo en nombre de hacértelo "fácil". Algunos de ellos incluso mantienen tu clave privada en el servidor, esto lo convierte esencialmente en cifrado del lado del servidor. Para nosotros es importante que la solución de extremo-a-extremo respete la libertad de elección de lxs Disrooters y a la vez que les otorgue un control total sobre las claves privadas, esté – tanto como sea posible – en consonancia con los estándares (no reinventar la rueda) y que sea totalmente de código abierto, de modo que otros proveedores dentro de la esfera de Librehost y más allá puedan adoptarla y crear almacenamiento y transmisión de correo cifrado de extremo-a-extremo interoperables.

Después de andar mirando y probar diferentes soluciones, nos hemos topado con un software llamado *"GPG Mail Gate"*. Aunque parece estar abandonado desde hace años, aparentemente resuelve todos los problemas de una sola vez.

* GPG - Utiliza el estándar GnuPG para el cifrado de correo. Esto significa que no solo funcionará dentro de Disroot, sino que podría ser usado para cifrar correos a otrxs y otrxs podrían cifrarlos fácilmente para ti.
* Subida de clave pública - Interfaz que permite subir tu clave pública. El servidor cifrará automáticamente todos tus correos entrantes con tu clave pública mientras que tu clave privada permanece secreta en tu dispositivo y el servidor de Disroot nunca la verá, tendrá acceso a ella ni ayudará a generarla. Además, con un servidor de claves en su lugar, tu clave pública podría ser utilizada por cualquiera para cifrar automáticamente los correos que te llegan. No hay necesidad de compartir las claves manualmente.
* Cifrado de los correos que enviaste - Todos los correos que envíes, incluso en texto plano, se almacenarán cifrados de extremo-a-extremo en el servidor.
* Configuración sencilla - Hace lo que necesita hacer y no requiere que quienes lo operen sean genixs.

### *Así que,* *¿cuándo lo estaríamos haciendo funcionar?*

Como el software está bastante desactualizado, nos gustaría primero encontrar una persona que lo mantenga, reescriba algunas partes del mismo y cree un mejor mecanismo, incrustado en el webmail para subir las claves. Además, nos gustaría que se ajustara a Autocrypt para hacerlo mucho más robusto.

#### *¿Quieres ayudar?*

Estamos buscando desarrolladores que estén dispuestxs a asumir el mantenimiento del proyecto, reescribirlo en python3 (o cualquier otro lenguaje a elección), y rehacer el servicio web de subida de claves. Estamos tratando de asegurar algunos fondos para ello también, así que podría pagarse algo más que una cerveza (tal vez un cajón de cervezas) para ayudar a financiar un trabajo que tiene el potencial de beneficiar a una red más grande de proveedores de correo. Si estás dispuestx a ayudar, por favor, ponte en contacto con nosotros.

## Temas

Como parte del trabajo que hemos iniciado el año pasado de brindar una experiencia más unificada, **Meaz** ha comenzado a trabajar en temas para algunos de los servicios. En este momento hemos "disrooteado" a **Hubzilla**, **Lufi**, **Nextcloud**, **Roundcube** _(próximamente...)_, y tenemos a **Searx**, **Privatebin** y **ConverseJS** casi listos también. Este es solo el inicio y abre las posibilidades para que no solo nos centremos en las paletas de color y elementos del diseño, sino también para trabajar en el diseño general de la aplicación, lo que mejorará la experiencia de usuarix (abreviado UX en inglés). Hasta ahora nos estamos divirtiendo haciéndolo y estamos emocionados por ver hasta dónde podemos llevarlo en el futuro.

## Cuota de voluntariado

**Disroot** ha recorrido un largo camino en nuestro esfuerzo por la independencia y la sostenibilidad económica. Desde la época en que pagamos todos los costos de nuestros bolsillos, pasando por el momento en que estos fueron finalmente cubiertos por las donaciones, hasta el punto en que empezamos a donar al Software Libre y de Código Abierto (SLyCA).

Decidimos que es hora de marcar un nuevo hito en ese viaje. **Disroot** es nuestra joya más preciada. Pasamos más tiempo en ella que en nuestros trabajos diarios (no se lo digan a nuestros jefes). Mientras la plataforma crece, también lo hace el tiempo necesario de dedicación. En este aspecto, nuestro objetivo final es alcanzar la independencia financiera y enfocarnos en **Disroot** como nuestra principal ocupación y mantenemos fieles a nuestras ideas iniciales respecto a nuestro "modelo de negocio" y no "vendernos". La situación actual no es sostenible y nuestra vida privada/familiar sufre por ello ya que el día solo tiene 24 horas y todavía tenemos que hacernos el tiempo suficiente para dormir y otras cosas (algo de lo que también carecemos).

Somos conscientes de que el camino que hemos elegido para alcanzar este ambicioso objetivo no es fácil. Pensamos que una aproximación económicamente más social – dar a las personas la posibilidad de decidir cuánto pueden y están dispuestas a contribuir financieramente al proyecto - es posible y puede generar un equilibrio sostenible entre aquellas que pueden permitirse pagar, aquellas que no pueden hacerlo y las que mantienen la plataforma y reciben un pago a cambio. Creemos que esta es la manera adecuada para llegar más lejos, pero el camino que conduce a ese objetivo final es largo y llevará mucho tiempo. Así que decidimos ir paso a paso.

La legislación holandesa permite a las fundaciones como la nuestra (en nuestro caso, sin fines de lucro) pagar una pequeña cuota por el trabajo voluntario. No puede ser más de 170 euros al mes o más de 1700 euros al año en total (es decir, aprox. 140 euros al mes). Viendo que nuestra situación financiera permite tales pagos, decidimos iniciar un programa de pago de cuota por voluntariado para Equipo Central de **Disroot**. El Equipo está formado actualmente por 4 personas.

Además de los costos y donaciones al SLyCA, este año también decidimos reservar 400 euros por mes tanto para gastos inesperados como para futuras inversiones en hardware. Una vez cubiertos los costos y gastos, si quedan al menos 140 euros, podríamos pagar una cuota de voluntariado a uno de los miembros del Equipo, si hay más de 280 euros podríamos pagar dos cuotas y así sucesivamente. Como red de seguridad, si no logramos reunir los fondos suficientes tres meses seguidos, dejaremos de pagar las cuotas.

Estamos muy contentos de anunciar que ya podemos pagar la cuota a dos de nuestros voluntarios 🎉. Nos esforzaremos por mantenerlos cubiertos y por conseguir que los otros se unan también. Una vez que estemos todos a bordo, no habrá ninguna fuerza que nos impida alcanzar otro hito como es conseguir que todos estén en la nómina.

Contamos con tu ayuda. Es importante comprender que cualquier cantidad ayuda. Si todos y todas contribuyeran con, al menos, 1 euro al mes por los servicios que brinda **Disroot**, no solo podríamos pagar todos los costos, invertir en mejor hardware y patrocinar el desarrollo de software libre y de código abierto, sino también el salario de los administradores. Así que no esperes hasta que puedas pagarnos un montón de dinero, empieza de a poco y crezcamos juntos.

Piensa en nosotros cuando compres tu bebida favorita cada mes en tu bar, café o en tu casa y solo "cómpranos" una bebida.
Mira nuestra [página de donaciones](https://disroot.org/donate)

## Reporte Anual 2019

Esto sí que se nos fue de las manos. Por alguna razón, redactar el informe anual… ¡nos llevó medio año! No estamos seguros de lo que pasó exactamente. Podríamos culpar al Coronavirus o simplemente al 2020 en conjunto, pero eso sería no hacernos cargo. Lo más probable es que la cantidad de trabajo que se ha acumulado y el constante estar atrás de otras prioridades haya causado que lo empujáramos al abismo de los trabajos pendientes. En fin, finalmente lo conseguimos y aquí está para [descargar](https://disroot.org/en/annual_reports/AnnualReport2019.pdf)

Esperamos que les guste. Como salió "ligeramente" demasiado tarde, es un buen momento para revisar lo más destacado del año pasado.

## Lo que está planeado para la segunda mitad del año

Queremos enfocarnos en mejorar la experiencia de usuarix. **Fede** está trabajando duro para actualizar todos los tutoriales y guías, y producir nuevas. Seguiremos trabajando en los temas y las mejoras en la experiencia de uso, pero principalmente nos centraremos en mejorar el servicio de correo electrónico. Queremos implementar una mejor protección contra el SPAM, nuevo webmail, cifrado de buzones y una mejor gestión de las solicitudes de dominios y alias personalizados.

Así que, a menos que extraterrestres invadan nuestro planeta, la Tierra sea tragada por un agujero negro, nos golpee Nibiru o cualquier otro evento inesperado que se presente este año, vamos a tener unos meses con mucho trabajo emocionante por delante.
