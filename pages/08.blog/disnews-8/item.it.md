---
title: 'DisNews #8 - Primo nuovo server e buon anno!'
date: '25-12-2022'
media_order: roos.jpg
taxonomy:
  category: news
  tag: [disroot, news, server, lacre, encryption]
body_classes: 'single single-post'

---
Ciao a tutti,
siamo quasi alla fine di un altro anno. Vogliamo augurare a tutti voi buon solstizio, fantastici festeggiamenti per il nuovo anno e un fantastico, sorprendente e incredibile 2023!
Ecco i punti salienti dell'ultimo mese e mezzo di Disroot.

## Nuovo server Roos

Avevamo già annunciato l'acquisto di nuovi server per Disroot. Il nuovo hardware sostituirà i nostri server attuali, che hanno già raggiunto la fine del loro ciclo di vita molto tempo. È quindi giunto il momento di aggiornarli. Poiché questa operazione richiede un certo lavoro, soprattutto per ridurre al minimo i tempi di inattività, abbiamo deciso di sostituire un server alla volta. Il 13 novembre abbiamo sostituito il primo server. Roos ha sostituito il vecchio server (Nelly), responsabile della gestione di tutti i database. Si tratta di un vero e proprio upgrade delle prestazioni hardware e, sebbene ci sia ancora del lavoro di ottimizzazione da fare, si può già sentire la differenza. Il nuovo server ci permetterà di migliorare ulteriormente le prestazioni e di aggiungere altri servizi.

## Lacre

Siamo molto lieti di annunciare che il nostro lavoro su Lacre (il nostro progetto di crittografia end-to-end delle caselle di posta elettronica) ha raggiunto la versione 0.1 ed è quasi pronto per un vero e proprio test di battaglia su Disroot. Come sapete, avevamo annunciato l'open beta test qualche settimana fa. Tuttavia, a causa di un bug che abbiamo riscontrato durante il closed alpha test sul server Disroot (sì, l'avevamo già implementato su Disroot, ma poi lo abbiamo disabilitato), abbiamo deciso di rimandare il tutto finché il problema non sarà risolto. @pfm e alcuni collaboratori della comunità stanno lavorando duramente per riprodurre il problema e trovare una soluzione (un ringraziamento speciale a *@darhma, @Onnayaku,* e *@l3o*). Il bug è molto difficile da risolvere perché è difficile da riprodurre e @pfm ci sta perdendo la testa. Ha lavorato instancabilmente e si è avvicinato alla soluzione. Ciò significa che presto proveremo a testare la crittografia end-to-end delle caselle di posta su Disroot, e forse la imposteremo in modo permanente. Se siete sul fediverso, date a [pfm](https://edolas.world/users/pfm) un po' di affetto!

## Akkoma/Pleroma, siamo su Mastodon? Cos'è questo fediverso?

Uno dei pilastri di Disroot è il supporto dei servizi federati. La federazione crea una rete decentralizzata di fornitori di servizi che consente agli utenti di interagire tra loro anche se ospitati su server diversi. Uno dei principali esempi di servizio federato è la posta elettronica. Gli utenti di Disroot possono inviare e-mail a qualsiasi altro utente ospitato altrove (ad esempio, tutanota, posteo, riseup o anche a quelli aziendali come gmail o yahoo). Non esiste un proprietario centrale del servizio di posta elettronica e quindi tutti i fornitori devono cooperare tra loro e consentire l'intercomunicazione. Nessuno può immaginare di dover creare account separati su Disroot, gmail e centinaia di migliaia di altri servizi per comunicare.

Fin dagli albori di Disroot, abbiamo fornito una rete sociale federata. Dal 2020, abbiamo cercato una nuova piattaforma di social network basata su un nuovo protocollo interoperabile chiamato ActivityPub. Abbiamo testato diversi servizi come Hubzilla, Mastodon, Pleroma e alla fine abbiamo scelto Akkoma. Akkoma è un fork del software Pleroma, che ha la caratteristica di essere compatibile con l'intera famiglia di protocolli ActivityPub.

ActivityPub è un protocollo di rete che consente a una moltitudine di servizi come il microblogging, il video hosting, la condivisione di file e così via, di comunicare tra loro. Ciò significa che non solo gli utenti possono comunicare con altri utenti Akkoma, ma anche con l'intero fediverso. Il fediverso è una rete che utilizza il protocollo ActivityPub e permette l'intercomunicazione tra diverse piattaforme, come ad esempio Mastodon/Pleroma/Akkoma (microblogging come twitter), PeerTube (pubblicazione di video come youtube), PixelFed (pubblicazione di foto come instagram) e molte altre. Immaginate di avere un account instagram, di interagire con i post di facebook o di commentare su reddit senza dover lasciare instagram o avere un account su reddit. ActivityPub è un protocollo in rapida crescita, con nuovi servizi che stanno nascendo nel fediverso.

Vorremmo invitare tutti voi nella nostra istanza di microblogging del fediverso chiamata [FEDIsroot](https://fe.disroot.org) Questo è il nostro primo ingresso nella rete e in futuro forniremo altri servizi del fediverso. La nostra istanza Akkoma funziona bene, anche se ha ancora qualche problema che dovremo risolvere nelle prossime settimane (per esempio, i nomi utente con i punti non sono accettati a causa di un problema che stiamo cercando di risolvere). Se riscontrate problemi con fe.disroot.org, se avete feedback da fornire, ecc. utilizzate i nostri canali di comunicazione per farcelo sapere. Il vostro feedback, soprattutto per un servizio giovane come questo, è molto importante.

**Desideriamo augurare a tutti voi un fantastico 2023. \o/**
