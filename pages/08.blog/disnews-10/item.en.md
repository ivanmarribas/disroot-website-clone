---
title: 'DisNews #10 - Migration to new servers complete; Lacre testing in horizon'
date: '13-06-2023'
media_order: White-blue-white.png
taxonomy:
  category: news
  tag: [disroot, news, baremetal, lacre, sequoia]
body_classes: 'single single-post'

---
Hi there Disrooters.

Time flies... 
Welcome to the 10th issue of DisNews. Half of the year is behind us and so far no alien invasion happened. Although the world is getting more and more crazy with each month. We hope you are all OK and taking care of yourself and your closest ones.

Here is what we have been busy with the last weeks.

First of all, something we forgot to mention in the last DisNews: the annual report for 2022 has been published! Check it [here](https://disroot.org/annual_reports/AnnualReport2022.pdf)

As some of you noticed we have not published the report for 2021. Of course we could blame COVID and the impact it had on our personal lives and how generally traumatic period it was and how hard to recall for annual report. Basically everyone would like to forget this year ever existed. The truth however is simplier than that: a dog ate our report draft.

That aside, check the report from 2022 and enjoy.

## New servers

As you could read in our previous posts (DisNews #5), we have purchased new hardware. From the beginning of this year we have been busy setting up the servers, installing them in the datacenter and migrating all the services. This was a time consuming process but it is almost complete. All bare metal servers are hanging in the racks all powered on. All virtual servers hosting all the services have been migrated to the new beasts. The only thing left to do is migrating the biggest filestores which are user's mailboxes and Nextclould's user files. As those are quite big datasets (not only because of their size but -most importantly- the amount of files), it will take quite some time to complete their migration. We want to make sure no dataloss occurs during this operation. Another important thing is doing it with minimal, and if possible imperceptible, downtime for Disrooters. Hopefully all data will be moved without you even noticing it. That said, **if you keep any important files on Nextcloud, during this period make sure to have an additional backup handy**.

With new hardware at our disposal we can now focus on performance improvements, which is what we will be busy with in coming months.

## Improvement in deployment

As the platform grows so is the responsibility for delivering services to all of you without issues caused by broken, untested rollout of new features or code updates. *"We test in production"* motto is something we no longer accept and so we are working on workflows that minimize possible issues. One of the things we have already introduced last year is Maintenance windows. Every second Tuesday we roll out updates we work on prior. This has provided us with workflow giving  a pulse to Disroot work. Additionally allows us for better planning on priorities and an overview of upcoming routine maintenance work. For you Disrooters, it provides insight into what is being updated as well as checking the past improvements via the [changelog page](https://disroot.org/changelog). To further improve the testing prior deployment we have moved from virtual machines environments on our local workstations and instead we have invested into small, physical desktop machines hosted locally in our houses running a very similar setup as the one on Disroot servers. After initial work in setting up, updating documentation (also something we are taking more seriously now) and knowledge transfer, we can prepare changes on our local machines, test on each other's before the update landing on Disroot production servers. Although still far away, it does bring us one little step closer to a situation where we would be able to provide the possibility for anyone out there to spin their own Disroot like node. After all we are now running three independent Disroot nodes already.

## Other notable changes

- Phasing out TLS1.1 - Time to phase out TLS1.1 has come long, long time ago. But each time we do it, we get someone complaining that their old device no longer works. This can't go forever and so 1st of July, we are phasing outy TLS1.1. Please consider upgrading your software (or hardware) if you are affected by this (Android v4.4, windows XP etc). 
- We have brought back app dashboard under [https://search.disroot.org](https://search.disroot.org) / [https://apps.disroot.org](https://apps.disroot.org) which was missing since we have moved from **Searx** to its fork called **Searxng**

- We have improved the color palette for our custom themes (specially dark) which we will work on to roll out on all services providing more uniform experience on entire platform

- Lacre - **@pfm** has managed to fix the bug which initially prevented us from running Lacre alpha tests on Disroot. It was quite a battle. Dragon has been slayed opening the door to the Disroot test again. Expect announcement about upcoming test of end-to-end mailbox encryption on disroot soon.

- Oh one more about Lacre. **@wiktor** one of the prominent developers of **Sequoia** has pushed to Lacre initial support for it. Sequoia is a more secure, robust re-implementation of PGP. We are very happy and thankful to **@wiktor** for his work!
