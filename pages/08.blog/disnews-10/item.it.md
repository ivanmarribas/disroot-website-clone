---
title: 'DisNews #10 - Completata la migrazione ai nuovi server; test Lacre all’orizzonte'
date: '13-06-2023'
media_order: White-blue-white.png
taxonomy:
  category: news
  tag: [disroot, news, baremetal, lacre, sequoia]
body_classes: 'single single-post'

---
Ciao Disrooters.

Il tempo vola... 
Benvenuti al decimo numero di DisNews. Metà dell'anno è alle spalle e finora non c'è stata nessuna invasione aliena. Anche se il mondo diventa sempre più pazzo ogni mese che passa. Speriamo che stiate tutti bene e che vi prendiate cura di voi stessi e dei vostri cari.

Ecco cosa abbiamo fatto nelle ultime settimane.

Prima di tutto, una cosa che abbiamo dimenticato di dire nell'ultima DisNews: è stato pubblicato il rapporto annuale per il 2022! Consultatelo [qui](https://disroot.org/annual_reports/AnnualReport2022.pdf).

Come alcuni di voi hanno notato, non abbiamo pubblicato la relazione per il 2021. Naturalmente potremmo dare la colpa al COVID e all'impatto che ha avuto sulle nostre vite personali e a quanto sia stato un periodo generalmente traumatico e difficile. La verità, tuttavia, è più semplice: un cane ha mangiato la bozza del nostro rapporto.

A parte questo, guardate il rapporto del 2022 e godetevelo.

## Nuovi server

Come avete potuto leggere nei nostri post precedenti (DisNews #5), abbiamo acquistato un nuovo hardware. Dall'inizio di quest'anno siamo stati impegnati nella configurazione dei server, nella loro installazione nel datacenter e nella migrazione di tutti i servizi. È stato un processo che ha richiesto molto tempo, ma è quasi completato. Tutti i server sono nei rack accesi. Tutti i server virtuali che ospitano tutti i servizi sono stati migrati sulle nuove macchine. L'unica cosa che resta da fare è la migrazione dei file più grandi, ovvero le caselle di posta elettronica degli utenti e i file utente di Nextclould. Poiché si tratta di insiemi di dati piuttosto grandi (non solo per le dimensioni, ma soprattutto per la quantità di file), ci vorrà un po' di tempo per completare la migrazione. Vogliamo assicurarci che non si verifichino perdite di dati durante questa operazione. Un'altra cosa importante è che l'operazione avvenga con tempi di inattività minimi, e se possibile impercettibili. Si spera che tutti i dati vengano spostati senza che l'utente se ne accorga. Detto questo, **se conservate file importanti su Nextcloud, durante questo periodo assicuratevi di avere un backup aggiuntivo a portata di mano**.


Con il nuovo hardware a disposizione possiamo ora concentrarci sul miglioramento delle prestazioni, che è ciò che ci impegnerà nei prossimi mesi.

## Miglioramento della distribuzione

Con la crescita della piattaforma cresce anche la responsabilità di fornire servizi affidabili. *Il motto "Testiamo in produzione "* è qualcosa che non accettiamo più e quindi stiamo lavorando a flussi di lavoro che riducano al minimo i possibili problemi. Una delle cose che abbiamo già introdotto l'anno scorso sono le finestre di manutenzione. Ogni due martedì lanciamo gli aggiornamenti su cui abbiamo lavorato in precedenza. 
Questo ci permette di pianificare meglio le priorità e di avere una panoramica dei prossimi lavori di manutenzione ordinaria. Per gli utenti di Disroot, fornisce una visione di ciò che è in fase di aggiornamento, nonché la possibilità di controllare i miglioramenti passati tramite la pagina [changelog](https://disroot.org/changelog). Per migliorare ulteriormente i test prima della distribuzione, abbiamo abbandonato gli ambienti di macchine virtuali sulle nostre postazioni di lavoro locali e abbiamo invece investito in piccole macchine desktop fisiche ospitate localmente nelle nostre case che eseguono una configurazione molto simile a quella dei server Disroot. Dopo il lavoro iniziale di impostazione, l'aggiornamento della documentazione (anche questo è un aspetto che stiamo prendendo più seriamente) e il trasferimento delle conoscenze, possiamo preparare le modifiche sulle nostre macchine locali, testarle l'una sull'altra prima che l'aggiornamento arrivi sui server di produzione Disroot. Sebbene sia ancora lontano, questo ci porta un piccolo passo avanti verso una situazione in cui saremo in grado di fornire a chiunque la possibilità di creare il proprio nodo Disroot. Ad ora stiamo già gestendo tre nodi Disroot indipendenti.

## Altre modifiche degne di nota

- Abbiamo riportato la dashboard delle app sotto [https://search.disroot.org](https://search.disroot.org) / [https://apps.disroot.org](https://apps.disroot.org) che mancava da quando siamo passati da **Searx** al suo fork chiamato **Searxng**.

- Abbiamo migliorato i colori dei nostri temi personalizzati (in particolare quelli scuri) e lavoreremo per implementarli su tutti i servizi, fornendo un'esperienza più uniforme sull'intera piattaforma.

- Lacre - **@pfm** è riuscito a risolvere il bug che inizialmente ci impediva di eseguire i test alfa di Lacre su Disroot. È stata una bella battaglia. Il drago è stato ucciso, aprendo di nuovo la porta ai test su Disroot. Aspettatevi presto un annuncio sul prossimo test della crittografia end-to-end delle caselle di posta su Disroot.

- Un'altra cosa su Lacre: **@wiktor** uno dei principali sviluppatori di **Sequoia** ha spinto verso il supporto iniziale di Lacre. Sequoia è una reimplementazione più sicura e robusta di PGP. Siamo molto felici e grati a **@wiktor** per il suo lavoro!
