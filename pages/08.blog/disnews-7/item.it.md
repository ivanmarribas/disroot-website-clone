---
title: 'DisNews #7 - Testing di Lacre su Disroot!'
date: '26-10-2022'
media_order: lacre.png
taxonomy:
  category: news
  tag: [disroot, news, mail, lacre, encryption]
body_classes: 'single single-post'

---
Ciao,

come avevamo comunicato nei post precedenti, negli ultimi mesi siamo stati impegnati con Lacre. Lacre è un software open source di crittografia delle caselle di posta elettronica basato su GnuPG. **@pfm** ha lavorato molto, superando i piani iniziali e portando il progetto ad un livello soddisfacente. Abbiamo infatti raggiunto uno stato di sviluppo che ci consente finalmente di testarlo su Disroot. Il nostro sogno si sta avverando! 

Vi informiamo che nel fine settimana tra il **4 e il 6 novembre** abiliteremo il server di posta Lacre su Disroot per il beta test aperto a tutti gli interessati. L'obiettivo di questo test è vedere come si comporterà Lacre su larga scala e in uno scenario di carico realistico. Vorremmo inoltre raccogliere feedback sull'esperienza degli utenti. Questo ci consentirà di far emergere eventuali criticità prima di estendere l'uso di Lacre a tutta la comunità.

Ecco alcune informazioni importanti sulla sperimentazione. **Si prega di leggerle attentamente prima di decidere di partecipare alla fase di test**.

- Venerdì 4 novembre, pubblicheremo informazioni dettagliate sull'evento all'indirizzo [https://disroot.org/lacre_test](https://disroot.org/lacre_test). Lì troverai tutti i dettagli su come configurare la crittografia con Lacre e come seguire gli aggiornamenti sulla fase di test. Saranno inoltre presenti informazioni sugli indirizzi di contatto in caso di emergenza, informazioni su eventuali problemi critici, feedback, ecc.

- **Questo test è rivolto a quei Disrooter che hanno familiarità con l'uso di GnuPG con la posta elettronica!** Una volta abilitato Lacre sul tuo account Disroot, tutte le email in arrivo verranno automaticamente crittografate per il tuo indirizzo email, per tutto il periodo di test, con la chiave pubblica che avrai fornito. **Se non sai cosa stai facendo o di cosa stiam parlando, non partecipare ai test o potresti perdere l'accesso alle e-mail**. Tieni comunque presente che puoi sempre utilizzare i tuoi alias @getgoogleoff.me e @disr.it.

- Lacre crittografa le e-mail in arrivo. Ciò significa che non crittograferà l'intera casella di posta, quindi le email ricevute prima della fase di test rimarranno in chiaro.

- Nel caso in cui il server di posta diventi instabile, causando un'interruzione o compromettendo il normale funzionamento del mail server, termineremo il test. Questo vorrebbe dire che avremo bisogno di altro lavoro prima che il servizio sia sufficientemente stabile. In questo caso chi vorrà comunque aiutare ulteriormente con i test, potrà contattarci per avere accesso al nostro server di test dove continueremo a lavorare per rendere migliore il software.

- Il ripristino in caso di smarrimento della chiave in questa fase non è possibile. Potrai naturalmente contattarci per chiedere aiuto, tuttavia non saremo in grado di decrittografare le e-mail ricevute nel periodo di test che andranno quindi perse.

Non vediamo l'ora di iniziare questo test! Indipendentemente dai risultati, siamo già felici e orgogliosi che questo progetto si sia potuto concretizzare. 

Un grandissimo ringraziamento a **Pfm** senza il quale non saremmo arrivati così lontano e a tutti coloro che hanno aiutato lungo. Un ringraziamento speciale va inoltre a **NLNET** che ha aiutato Lacre e molti altri progetti (https://nlnet.nl/project/).

Incrocia le dita e imposta un promemoria. Il **4 novembre** controlla la pagina e unisciti a noi!
