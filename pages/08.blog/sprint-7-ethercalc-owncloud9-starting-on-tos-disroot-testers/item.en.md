---
title: '[Sprint 7] – Ethercalc, Owncloud9, starting on TOS, Disroot testers'
date: 04/11/2016
taxonomy:
  category: news
  tag: [disroot, news, calc,tos, testing]
body_classes: 'single single-post'

---

**Dear Disrooters,**

Spring is here! At least in Amsterdam.. Everything is waking up and life is pumping back into everything.
We are very happy to announce the long awaiting changes finally here to welcome this new season. Apart from updating all Disroot applications to the latest versions, we’ve also been working on some new things. Check it out!

## New Services and Features:

**[https://calc.disroot.org](https://calc.disroot.org)** – One of the things we were still missing in Disroot was collaborative spreadsheet. Not any more. Ethercalc is a pretty decent piece of software for this purpose. Check it out and give us your feedback. Oh, and be sure to save your work elsewhere when you’re done, because all spreadsheets that aren’t touched for over three months will be deleted from our servers.

**ownCloud9** – New version of ownCloud is here. We are always very excited of new versions of software that bring a lot of changes, and this update of ownCloud is no different. Here are the highlights:

  - **New calendar and contacts app.** Totally rewritten and included in the core of ownCloud. Looks sleek and will guarantee stable performance. !!Important note!! The url to your contacts and calendars sync has changed to [https://cloud.disroot.org/remote.php/dav](https://cloud.disroot.org/remote.php/dav) (for both contacts and calendar) so please update your devices.
  - **Tags and Comments** – Now you can tag and comment on the files you’re sharing or working together with others.
  - **Activity feed and Notifications** – to keep you updated on what is happening with your data.
  - **New chat interface** – the interface and the whole backend for XMPP chat went through a lot of changes. The UI has been redesigned, you can send and receive images inline of your chat window, and we are currently working on implementing audio/video conversation support (stay tuned). Please give it a shot and tell us your feedback at our chat disroot@chat.disroot.org
  - **Pad and spreadsheet app** – now you can manage and share your pads and spreadsheets just as any other file on your ownCloud storage. You can create new pads and spreadsheets by clicking on a plus icon in your files window just as if you would for new folder or a text file. The app will lead you to either pad.disroot.org or clac.disroot.org where you can work on your file. The app provides extra security by generating a long file name made out of random characters. This still means that anyone who knows the name could view and edit your file, but given the fact that the file names are random (e.g. 54pfvjt7veom) it’s very unlikely that unauthorised person or bots will find it. As mentioned above, pads or spreadsheets that haven’t been accessed for over 3 month, will be deleted. Detailed howto can be found here: [https://forum.disroot.org/t/owncloud-using-collaborative-pads-and-spreadsheets](https://forum.disroot.org/t/owncloud-using-collaborative-pads-and-spreadsheets)

**Xmpp** – Chat now supports file upload via http. To keep all different xmpp clients compatible with one another our server will automatically upload the file via http server so that everyone you send the file can see it no matter what client they are using. The files will be deleted after 7 days of upload. For detailed list of plugins currently setup on the server check this page: https://disroot.org/chat/

## New policy: time limit on pads and spreadsheets

Starting from the 1st of June, we will introduce a new policy regarding pad’s expiry date. All pads or spreadsheets on [https://pad.disroot.org](https://pad.disroot.org) / [https://calc.disroot.org](https://calc.disroot.org) that are not edited for over 3 months will be automatically deleted. If you find this decision ludicrous, please let us know via email or (preferably) via the [disroot forum](https://forum.disroot.org) where we can have an open discussion to persuade one another.

## Terms of services, privacy policy.

We have started working on Terms of services and Privacy policy documents. All this juridical administrative mambo jumbo is really not our cup of tea, which is why it takes us so long. We ‘sampled’ documents from other similar projects and tried our best to put some TOS text together. We will continue to work on it during the up coming sprint and we hope to have it ready to publish within next three weeks. We would very much appreciate help with those documents, so whether you have some experience writing this type of documents or not, please take a minute to review what we have so far:
https://pad.disroot.org/p/disroot-tos

## Disroot testing group.

First of, we’ve started a Disroot chat room. A place to talk directly to us, but also to find other disrooters, discuss disroot features or just complain about shit not working. check it out, add it to your bookmarks and come to say hi: disroot@chat.disroot.org

We are constantly looking to improve and expand our services, interface and usability. We are only human, so errors, bugs, problems, spelling mistakes or confusing functionalities are unavoidable. One way to contribute to the Disroot project is to become a hunter; Use the software, browse our website, try out everything and let us know if you bump into walls. We would also like to have a wider group that will try out new applications before we make them public.

In order to gather all those user experiences and feedback on new functionalities, we are going to start a testing group, with a dedicated forum, shared cloud and access to new, not-yet-public, services . We might send out specific requests to test new features or just gather your feedback, and open a discussion to really understand how we can improve usability.

If you are interested in joining our testers crew please send email to disroot-testers_at_disroot.org (you must have a Disroot account to opt-in)
