---
title: 'La nostra posizione su Meta Threads'
date: '07-07-2023'
media_order: threat.png
taxonomy:
  category: news
  tag: [disroot, news, meta, threads]
body_classes: 'single single-post'

---

Meta (la società precedentemente nota come Facebook e proprietaria di Instagram, WhatsApp e del "social network" Facebook) ha appena lanciato una nuova piattaforma chiamata Threads che mira a "uccidere" Twitter. Ogni guerra (reale o virtuale) fa vittime. E in questa che viene combattuta dalle grandi aziende per il controllo delle comunicazioni, pensiamo che il fediverso, e soprattutto Mastodon, saranno tra le vittime.

Quando Meta ha annunciato il suo lancio, ha anche dichiarato che la nuova piattaforma sarebbe stata compatibile con il protocollo ActivityPub. Lo stesso protocollo che alimenta l'intero Fediverso. Ciò significa che Threads sarà interoperabile con gli altri server della rete, compreso il nostro. Per alcuni sembra un sogno che si avvera: finalmente tutti, indipendentemente da dove "vive" il loro account, saranno in grado di comunicare tra loro. Sulla carta sembra tutto fantastico....
Tuttavia, se teniamo presente ciò che la storia ci ha insegnato, sembra che si stia ripetendo la storia della pecora che invita il lupo a cena.

Meta è una di quelle aziende che hanno una reputazione terribile e ben nota. È molto probabile che la maggior parte di voi sia qui perché cercava di sfuggire alle "piattaforme" delle "grandi aziende tecnologiche" come Meta. Ebbene, indovinate un po': come il lupo della storia, le grandi aziende stanno di nuovo mettendo piede alla nostra porta.

Riteniamo che l'ingresso di Meta nel Fediverso avrà conseguenze molto negative a lungo termine. Threads diventerà fin dal primo giorno il nodo più grande e influente della rete federata. Di fatto, sarà più grande di tutti i server attuali messi insieme. E, almeno per noi, questo significa che potrà prendere il comando nello sviluppo di nuove funzionalità e costringere gli altri partecipanti a implementarle (nel bene e nel male). Che si tratti di funzionalità che attendiamo da tempo (come la migrazione degli account), di altre che danneggiano la privacy degli utenti o dell'introduzione di piattaforme pubblicitarie, che prima o poi gli altri dovranno implementare per rimanere compatibili con il resto della rete. 

Naturalmente, a questo punto possiamo solo speculare sui piani che Meta ha per noi e su come si svolgerà il tutto, ma visti i loro precedenti, non possiamo aspettarci nulla di buono. Il fatto che ActivityPub sia un protocollo aperto non impedisce a Meta di utilizzarlo, ma come rete di server all'interno del Fediverso non abbiamo bisogno di impegnarci con loro e di permettergli di entrare nei nostri spazi.

Per questo motivo abbiamo deciso di non voler collaborare con Meta a nessun livello e quindi di non federarci con loro. Non federarsi con Threads è probabilmente l'unico modo, per un'istanza o un'organizzazione con i valori che noi sosteniamo, di esprimere la propria posizione e dire che non è d'accordo e non vuole avere nulla a che fare con ciò che Meta fa e come Meta lo fa.

L'unico valore aggiunto di Threads sono i suoi utenti. Ci piacerebbe vedere l'intera popolazione terrestre utilizzare ActivityPub, perché risolve il problema della centralizzazione e del monopolio e dà l'opportunità a piccole organizzazioni, gruppi, famiglie o individui di ospitare autonomamente la propria piattaforma sociale e di avere il controllo sui propri dati.

Crediamo però che questo cambiamento debba avvenire in modo organico, man mano che le persone comprendono i concetti di federazione e decentralizzazione, li considerano una parte importante della loro vita digitale e costruiscono una rete equilibrata e autogestita, libera dalle influenze delle grandi aziende tecnologiche come Meta.
Vogliamo tutti nel Fediverso, ma non alle condizioni dettate da Meta, Alphabet o altri!

Cosa significa questo per i Disrooters?
Che quando Threads entrerà nel Fediverso, non potrete interagire con i suoi utenti. Il nostro server non si federerà con Meta. Se questa decisione rappresenta un problema per voi, grazie alla natura (ancora) eterogenea della rete, potrete sicuramente trovare una nuova casa su un server che ha deciso di concedere a Meta il beneficio del dubbio.

Pur avendo deciso di non federarci con il nuovo prodotto di Meta, rispettiamo pienamente l'autonomia e le decisioni che altri potrebbero prendere. Le nostre "armi" sono e sono sempre state puntate contro i grandi signori della guerra tecnologica. Non le puntiamo di certo contro gli altri membri del Fediverso. Non bloccheremo le istanze che hanno deciso di federarsi con Meta. Hanno il diritto di farlo e, anche se non siamo d'accordo con la loro scelta, accettiamo la loro decisione.

Allo stesso tempo, desideriamo e dobbiamo evitare di metterci l'uno contro l'altro all'interno della rete che abbiamo lavorato così duramente per sostenere. Se c'è una cosa che Meta e gli altri fanno molto bene, è proprio promuovere la frammentazione della rete in fazioni che si combattono a vicenda.

Non sappiamo cosa ci riservi il futuro, ma una cosa è certa: questa mossa da parte di Meta dimostra che le reti federate sono il futuro e le multinazionali lo vedono come una minaccia per il loro sistema di produzione di denaro. Non vengono in pace, ma per conquistare la torta prima che lo facciano gli altri e prendere il controllo per mantenere il loro monopolio e i loro profitti. Dobbiamo mantenere le nostre basi e continuare a fare un lavoro straordinario senza di loro.

Non abbiamo bisogno di loro.

**Teniamo fuori i lupi!**
