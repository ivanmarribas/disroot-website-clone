---
title: 'Our position on new Meta ThreaTs'
date: '07-07-2023'
media_order: threat.png
taxonomy:
  category: news
  tag: [disroot, news, meta, threads]
body_classes: 'single single-post'

---

Meta (the company formerly known as Facebook and owner of Instagram, WhatsApp and the Facebook "social network") has just launched a new platform called Threads that aims to "kill" Twitter. Every war (real or virtual) brings casualties. And in this one being waged by large corporations for control of communications, we think the Fediverse, and especially Mastodon, will be among those casualties.

When Meta announced its launch, it also stated that the new platform would be compatible with the ActivityPub protocol. The same protocol that powers the entire Fediverse. This means that Threads will be interoperable with other servers on the network, including our own. For some, it sounds like a dream come true: finally everyone, regardless of where their account "lives," will be able to communicate with each other. Sounds great on paper, yeah...

However, if we keep in mind what history has taught us, we seem to be repeating the tale of the sheep inviting the wolf to dinner.

Meta is one of those corporations that has a terrible and well known reputation. And it is very likely that most of you are here because you were looking to escape the "platforms" of "big tech corps" like this one. Well, guess what... Like the wolf in the story, the big corporations are getting a foot in our door again.

We believe that Meta's entry into the Fediverse will have very negative long-term consequences. Threads will become from day one the largest and most influential node in the federated network. In fact, it will be larger than all the current servers combined. And at least for us, this means that they can take the lead in developing new features and force other participants to implement them (for better or worse). Whether it be features that we have been waiting for a long time (such as account migration), others that harm users' privacy or the introduction of advertising platforms, which others will sooner or later have to implement in order to remain compatible with the rest of the network. The rest of the network, in this scenario, would be that other single server with billions of users.

Of course, at this point we can only speculate on what plans Meta has for us and how it will all play out, but given their track record, we cannot expect it to come in good faith. The fact that ActivityPub is an open protocol does not prevent Meta from using it, but as a network of servers within the Fediverse we do not need to engage with them and allow them into our spaces.

That is why we have decided that we do not want to cooperate with Meta at any level and therefore not federate with them. Not to federate with Threads is probably and simply the only way, for an instance or organization with the values we stand, to express their position and say that they do not agree and do not want to have anything to do with what Meta does and how Meta does it through its products: promoting and inducing the generation of interactions and conditioned habits for the production and extraction of data.

The only added value that Threads is bringing to the table is their users. We would love to see the whole Earth's population using ActivityPub, as it solves the problem of centralization, monopoly and gives the opportunity to small organizations, groups, families or individuals to self-host their social platform and have control over their data.

However, we believe that this change has to happen organically, as people understand the concepts of federation and decentralization, consider them an important part of their digital life and build a balanced and self-owned network, free from the influences of the big tech corporations. Until then, the billions of users provided by Meta will destroy that concept.

We do want all of you in the Fediverse, but not on the terms dictated by Meta, Alphabet or others!

So what does this mean for you, dear Disrooters?

That when Threads enters the Fediverse, you will not be able to interact with users on it. Our server will not federate with Meta. If this decision presents a problem for you, thanks to the (still) diverse nature of the network, perhaps you can find a new home on a server that has decided to give Meta the benefit of the doubt.

While we have decided not to federate with Meta's new product, we fully respect the autonomy and decisions of others who choose to federate. Our "guns" are and always have been aimed at the big tech warlords. We are not aiming them at other fellow members of the Fediverse. We are not going to block instances that decided to federate with Meta. They have the right to do so, and while we may not always agree with their choice, we fully support their decision.

At the same time, we hope, wish and need to not turn against each other within the network that we have worked so hard to sustain. If there is one thing Meta and the others do very well, it is to promote the fragmentation of the network into sides fighting each other before Threads even becomes a real **THREAT**. So support your admins, support your users, appreciate the diversity of the network and make use of it.

We do not know what the future brings, but we do know one thing for sure. This Meta movement proves that federated networks are the future and corporations see it as a threat to their money making machinary. They are not coming in peace, but to take over the cake before others do and take control to maintain their monopoly and profits. We must keep our grounds and continue to do amazing work without them.

We don't need them. We have done just fine so far.

**Let's keep the wolves out!**
