---
title: 'DisNews #9 - Limpieza general; TOR; Forgejo'
date: '20-03-2023'
media_order: Protests_against_war_in_Ukraine_in_Moscow.jpg
taxonomy:
  category: noticias
  tag: [disroot, noticias, tor, forgejo, discourse, snappymail, taiga]
body_classes: 'single single-post'

---

Hola,

este párrafo iba a referir, originalmente, al hecho de que queríamos ser más constantes este año y enviar las DisNews regularmente cada mes. No tendría nada de malo excepto que, por alguna inexplicable razón, este borrador quedó a la espera de ser publicado por más de dos meses. Hay tanto para corregir...

El lado positivo es que todavía nos quedan unos meses de 2023 para mejorar eso. En fin, esto es lo que hemos estado haciendo.

## Finalizando servicios (limpieza de primavera adelantada)

El año nuevo trae nuevas energías y nuevas ideas. Decidimos comenzarlo haciendo una temprana limpieza primaveral. Una de las cosas en las que nos gustaría enfocarnos este año es hacer de Disroot una plataforma más unificada. Con mucha frecuencia, los servicios duplican algunas funciones, dando lugar a productos poco desarrollados o dividiendo a la comunidad en nichos más pequeños. Esto, a su vez, requiere trabajo de mantenimiento adicional y a menudo resulta en un estado poco pulido y abandonado.

Taiga es uno de esos servicios. Durante años sirvió como una buena alternativa de tablero de gestión de proyectos. Es una gran herramienta para grupos relacionados al software ofreciendo todo lo mejor que el [SCRUM](https://es.wikipedia.org/wiki/Scrum_(desarrollo_de_software)) y el [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)) pueden dar. Sin embargo, desde hace unos años hay algunas buenas alternativas que se ajustan mejor a nuestra base de usuaries (y a nosotros mismos). Taiga es grandioso pero está pensado para grupos de desarrollo profesional. Para tableros Kanban simples o lista de pendientes, Deck y Tareas de Nextcloud son más adecuados y para todo lo relativo a tableros de desarrollo, el de incidencias de Forgejo es más que suficiente. Además, considerando que nuestra instancia de Taiga ha sido utilizada por muy poca gente en los últimos años y más recientemente ese número ha disminuido, no vemos que tenga mucho sentido seguir manteniéndola. Esos recursos pueden emplearse mejor en otra cosa. Así que, hemos decidido terminar con Taiga a partir del 1ro de Julio de 2023.

El Foro ha sido siempre un proyecto fallido. La idea principal era proporcionar a las personas usuarias una plataforma para crear sus propios grupos de discusión públicos y privados. Sin embargo, debido a limitaciones del programa esto resultó ser muy difícil de implementar y requería de funcionalidades complejas que nunca despegaron apropiadamente. Aparte de unos pocos grupos privados, el foro fue usado principalmente como un espacio para soporte de Disroot. Este no era el propósito pretendido del software. De todos modos, incluso para este caso de uso, nunca funcionó suficientemente bien. Ni tuvo mucha participación de la comunidad para ayudar a otras personas con problemas relacionados con Disroot (la mayoría de Disrooters actives se juntan en la sala de chat y el Fediverso), y se creó otro lugar más en el que nosotros, los admins, teníamos que mantener la mirada y responder. Para terminar con esto, también decidimos brindar por el momento solo dos puntos de contacto para soporte/sugerencias:

- por correo: la manera más privada para contactarnos con los inconvenientes que pudieran tener;

- el tablero de incidencias de Git: donde pueden seguir los desarrollos actuales, dejar opiniones, compartir problemas, sugerencias, etc. Este tablero es público para que cualquiera pueda verlo y seguirlo.

Así que a partir de ahora, ya no se podrán ver tópicos en Discourse ni crear nuevas cuentas. Estamos pensando en algún servicio de remplazo que se ajuste a la idea de un espacio de discusión comunitario auto-regulado, pero aún no hemos decidido qué camino seguir. Probablemente tengan noticias nuestras al respecto pronto.

El último servicio que planeamos cerrar es Snappymail. Desde hace ya varios meses, hemos estado brindando dos clientes web de correo. Uno es el mencionado Snappymail, que algunes de ustedes conocerán mejor si son Disrooters de la vieja escuela (accesible en https://mail.disroot.org/ ) y el otro llamado Roundcube, disponible en https://webmail.disroot.org, que viene siendo nuestro webmail por defecto desde hace ya algún tiempo para quienes recién llegan. Proveer dos soluciones de correo permite a todas las personas usuarias cambiar fácil y fuidamente de uno al otro y también nos ayudó a pulir el remplazo. Hasta ahora estamos muy contentos con Roundcube: se integra bien con la instancia Nextcloud de Disroot, por lo que todos sus contactos en la Nube están disponibles en el correo web; tiene una buena funcionalidad de filtros (que es importante para muchas personas) y para quienes quieran cifrar sus correos, también funciona bien con el complemento para el navegador Mailvelope. Pueden ver la guía [aquí](https://howto.disroot.org/es/tutorials/email/webmail/roundcube/encryption). Así que llegó el momento de decidir por una solución de correo web. A partir del 1ro de Junio, Snappymail será desconectado definitivamente y Roundcube será la única solución para todes. Quienes aún usan Snappymail y no han hecho la transición, por favor, lean los tutoriales sobre cómo [migrar](https://howto.disroot.org/es/tutorials/email/webmail/migration).


## TOR

Si decimos que ofreceremos servicios [onion](https://es.wikipedia.org/wiki/.onion) no tienen que recordárnoslo cada cinco años :). Sí. Finalmente está aquí. Es uno de los primeros pedidos que por alguna razón fuimos posponiendo en la lista de pendientes una y otra vez. Ha llegado también el momento de ponerse con esto y hemos lanzado la primera tanda de direcciones onion. Actualmente la mayoría de los servicios web la tienen (y también es autodetectada por el navegador Tor). Lo siguiente son cosas como XMPP y el correo electrónico. Esperemos que no tome otros tantos años.

## Cambiando de Gitea a Forgejo

Una y otra vez el ánimo de lucro lo destruye todo. Gitea, el programa de control de versiones que ofrecíamos en https://git.disroot.org hace algunos meses se convirtió en una entidad comercial. La motivación de esta decisión fue proporcionar una base financiera estable para el equipo de desarrollo. Y no habría nada malo en ello y apoyaríamos este movimiento al 100%, porque pensamos que la gente que pone su tiempo, esfuerzo y amor a los proyectos de código abierto debe ser compensada económicamente por su trabajo en beneficio de todas las personas (esta es la razón por la que tratamos de compartir el excedente de las donaciones que recibimos a esos proyectos). Sin embargo, muy a menudo la decisión de orientarse en la dirección de monetizar un proyecto simplemente se hace mal, y la historia de Gitea es, desafortunadamente, un ejemplo de ello. En resumen, el equipo principal de Gitea anunció la creación de una nueva empresa con fines de lucro y se apoderó de todos los activos (incluido el nombre, el logotipo, etc.) sin consultar previamente a la comunidad. Dado que Gitea como proyecto no fue desarrollado solo por este equipo, sino por una comunidad más amplia de colaboradores, la gente se sintió traicionada. Esta fue la razón por la que algunos de los miembros implicados decidieron bifurcar el proyecto y crear un verdadero derivado comunitario de Gitea llamado Forgejo. Por lo que decidimos seguir esa idea y apoyarlos cambiando nuestra instancia a Forgejo. Pueden leer más sobre la situación [aquí](https://forgejo.org/2022-12-15-hello-forgejo/) (en inglés). ¡Poder para el Pueblo!


## Recordatorio amistoso sobre el cuidado de las contraseñas

Queremos hacer también un breve recordatorio sobre la contraseña de la cuenta de Disroot. Últimamente hemos estado recibiendo un montón de preguntas y solicitudes de usuaries que pierden sus contraseñas. Es necesario recordar que **las contraseñas y cuentas son responsabilidad de las personas usuarias**. Como no pedimos más información que la estrictamente necesaria para la operación del servicio, no tenemos manera de determinar a quiénes les pertenecen legítimamente las cuentas y por consiguiente **nunca restablecemos las contraseñas**. Esto quiere decir que la cuenta puede perderse. Así que aquí van algunas sugerencias.

Primero, la mejor forma de mantener sus contraseñas a salvo es utilizar herramientas como gestores de contraseñas (KeePassXC, KeeWeb, QTPass, Pass o Bitwarden). Son salvavidas.

Segundo, si todavía la pierden por alguna razón, la pueden restablecer yendo a https://user.disroot.org. PERO pueden hacerlo solo si ya han configurado:
* las preguntas de seguridad,
* o un correo secundario donde el correo de restablecimiento pueda ser enviado.

Así que, si quieren mantenerse a salvo, no duden en ir a https://user.disroot.org y configurar eso.  
Aquí pueden aprender cómo hacerlo:  
https://howto.disroot.org/es/tutorials/user/account/administration/ussc
