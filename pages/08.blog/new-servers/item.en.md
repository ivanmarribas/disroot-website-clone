---
title: 'New Servers - Extra storage - Nextcloud 13'
media_order: twoServers.jpg
published: true
date: '14-03-2018 22:38'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

### Important!
**Changes in TOS:**

Due to growing abuse of our services (mainly email) by businesses, who find it convenient to use free volunteer based service provider for the purpose of profit generation, we have added the following section to our Terms Of Service:


> **Using Disroot services for commercial activities**<br>
Disroot is a non-profit organization providing services for individuals on "pay as you wish" basis. Because of this structure we see using Disroot services for commercial purposes as abuse of the service and it will be treated as such.
- Trading any Disroot services with a third party is prohibited.
- Using email for purposes such as "no-reply" type of accounts for a business will be blocked without a warning once such activity is detected
- Sending bulk emails, including but not limited to marketing and advertising, for business purposes, will be treated as spam and blocked upon discovery without any prior notice.
- Using Disroot services for financial gain, including but not limited to trading or managing sales, is not tolerated. Accounts created for the purpose of generating profits will be subject to termination upon inquiry.
- Using Disroot services for any other commercial activity will be examined per case and the decision on terminating such accounts will be based upon communication with the account holder and the type of the activities in question.


The new section will be put in practice starting from 1st of April. If you have questions, or are not sure if you fall under this new definition, please contact us.

<advertisement\> Any legit business that wish they could continue using such awesome service and support, please contact us per email to hear about our commercial offerings. </advertisement\>

### New servers
As some of you already know and have experienced, last months we were busy deploying new servers. This greatly improves overall performance of the Disroot platform as well as creates a very much needed resource buffer for the upcoming months. So without further ado meet:

Knopi - our main server - Dell R410:
  - 32GB RAM
  - 2x L5630
  - 4x 8TB in HWRAID10 (16TB in totoal)

Nelly  - our database server - Dell R610:
  - 88GB RAM
  - 2xE5520
  - 6x512GB SSD in HWRAID10 (1.5TB Total)


The operation has taken much more time then expected and was not without mistakes, causing downtimes that could have been avoided. The sleepless nights spent in the data-center gave us a lot of experience that we surely will use next time we need to move or upgrade the infrastructure :) We would like to say we're very sorry for causing any inconvenience, and would like to thank the Disroot community for supporting us in those hard times.

### Nextcloud 13
The long awaited update to Nextcloud 13 has reached Disroot. What's new? Apart from typical performance boost, there are two major features lots of you were waiting for:
  - Nextcloud Talk - Upgrade to the app formerly known as Spreed Calls (video/audio conferencing). Not only it has now a built in chat, but also a dedicated mobile app for managing your calls. This is still very fresh and we are looking forward to future integration with other Nextcloud apps. The rumor says Nextcloud Talk will be integrated with Matrix and XMPP. We are keeping our fingers crossed.

  - End to End encryption - Is now working with Nextcloud files. Though still in alpha state and only available on Android sync client, it is possible to end-2-end encrypt your files (folder based). It is wise to be aware of its main drawback - not being able to use web-interface to access the e2e encrypted folder (it's encrypted all-right). **Please read the documentation and remember this is an early stage feature so keep a good backup of the files you intend to encrypt.**

  https://nextcloud.com/endtoend/

### Extra storage space
Running out of space on your cloud or email? Finally we've got you covered. As of now you can request extra space for cloud or mailbox. Like we communicated in our previous announcement, the price per GB is 0.15euro/month and we will be first offering 14GB, 29GB, 54GB (total space) expansion. You can submit your requests via [this](https://disroot.org/en/forms/extra-storage-space) form.

### Finances
We have updated our finance chart. It looks very good! Thanks a lot to everyone who contributed so far! We were very excited to see that for the first time we were able to pay Disroot's monthly bills entirely from received donations. We hope we'll continue being surprised in the coming months too - so don't stop "buying us coffee" just yet. Furthermore, if we need to expand, upgrade hardware, get more servers (to be able to host more users and more services) we will need bigger funds. Remember, there is no such things as 'free' service and cloud is just a fancy word for someone else's computer.

### New Services?
We are getting a lot of requests for new services. We are also thinking of some additions ourselves. Naturally we can't facilitate all of them right away because of the hardware resources needed as well as work hours put into maintaining them. Given the limited resources we need to decide what is the most needed/anticipated service you would like to see on Disroot. We made a short list of services we are considering, have a look at [this](https://poll.disroot.org/ff3Qbrs1Kh74pHtN) poll and vote for your fav (no guarantees though). The poll will close on the 15th of April.

<br><br>

**In the coming month we are going to take things a little easier and focus on small maintenance and general spring cleaning.**

**We would like to wish everybody great spring celebrations, and for those of you in the southern hemisphere - cozy autumn nights.**
