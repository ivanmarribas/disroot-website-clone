---
title: 'Nouveaux serveurs - Stockage supplémentaire - Nextcloud 13'
media_order: twoServers.jpg
published: true
date: '14-03-2018 22:38'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

### Important!
**Changements dans les CGU:**

En raison de l'abus croissant de nos services (principalement par courriel) par les entreprises, qui trouvent pratique d'utiliser un fournisseur de services bénévole et gratuit dans le but de générer des profits, nous avons ajouté la section suivante à nos Conditions d'utilisation :

> **Utiliser les services de Disroot pour des activités commerciales**<br>
Disroot est un organisme à but non lucratif qui fournit des services aux particuliers sur la base de la "paiement à la carte". En raison de cette structure, nous considérons l'utilisation des services Disroot à des fins commerciales comme un abus du service et cela sera traité comme tel.
- L'échange de tout service Disroot avec une tierce partie est interdit
- L'utilisation du courrier électronique à des fins telles que les comptes de type "pas-de-réponse" pour une entreprise sera bloquée sans avertissement une fois qu'une telle activité est détectée.
- L'envoi de courriels en masse, y compris, mais non limité au marketing et à la publicité, à des fins commerciales, sera traité comme du pourriel et bloqué lorsque c'est découvert sans préavis.
- Utiliser les services Disroot à des fins lucratives, y compris, mais sans s'y limiter, la négociation ou la gestion des ventes, n'est pas toléré. Les comptes créés dans le but de générer des profits seront sujets à résiliation après enquête.
- L'utilisation des services Disroot pour toute autre activité commerciale sera examinée au cas par cas et la décision de fermer ces comptes sera basée sur la communication avec le titulaire du compte et le type d'activités en question.

La nouvelle section sera mise en pratique à partir du 1er avril. Si vous avez des questions ou si vous n'êtes pas certain de savoir si vous êtes visé par cette nouvelle définition, veuillez nous contacter.

<annonce\> Toute entreprise légitime qui souhaite pouvoir continuer à utiliser un service et un support aussi formidable, veuillez nous contacter par e-mail pour en savoir plus sur nos offres commerciales. </annonce\>

### Nouveaux serveurs
Comme certains d'entre vous le savent déjà et l'ont expérimenté, ces derniers mois, nous étions occupés à déployer de nouveaux serveurs. Cela améliore considérablement les performances globales de la plate-forme Disroot et crée un tampon de ressources dont nous avions grand besoin pour les mois à venir. Alors, sans plus attendre, rencontrez:

Knopi - notre serveur principal - Dell R410:
  - 32Go RAM
  - 2x L5630
  - 4x 8To dans HWRAID10 (16To au total)

Nelly  - notre serveur de base de données - Dell R610:
  - 88Go RAM
  - 2xE5520
  - 6x512Go SSD dans HWRAID10 (1.5To au total)


L'opération a pris beaucoup plus de temps que prévu et n'a pas été sans erreurs, causant des temps d'arrêt qui auraient pu être évités. Les nuits blanches passées dans le centre de données nous ont apporté beaucoup d'expérience que nous utiliserons sûrement la prochaine fois que nous devrons déménager ou mettre à niveau l'infrastructure :) Nous voudrions dire que nous sommes vraiment désolés d'avoir causé des désagréments, et nous aimerions remercier la communauté Disroot de nous soutenir dans ces moments difficiles.

### Nextcloud 13
La mise à jour tant attendue de Nextcloud 13 a atteint Disroot. Quoi de neuf ? En dehors de l'augmentation habituelle de performance , il y a deux caractéristiques majeures que beaucoup d'entre vous attendaient :
 - Nextcloud Talk - Mise à jour de l'application anciennement connue sous le nom de Spreed Calls (vidéoconférence/audio conférence). Non seulement elle dispose désormais d'un chat intégré, mais aussi d'une application mobile dédiée à la gestion de vos appels. C'est encore tout nouveau et nous attendons avec impatience l'intégration future avec d'autres applications Nextcloud. La rumeur dit que Nextcloud Talk sera intégré avec Matrix et XMPP. Nous croisons les doigts.

 - Chiffrement de bout en bout - Fonctionne maintenant avec les fichiers Nextcloud. Bien que toujours dans l'état alpha et disponible uniquement sur le client de synchronisation Android, il est possible de chiffrer vos fichiers de bout-en-bout (basé sur les dossiers). Il est important d'être conscient de son principal inconvénient : ne pas être en mesure d'utiliser l'interface web pour accéder au dossier chiffré de bout-en-bout (il est crypté tout droit). **Veuillez lire la documentation et n'oubliez pas qu'il s'agit d'une fonction à un stade précoce, alors conservez une bonne sauvegarde des fichiers que vous avez l'intention de chiffrer.**

  https://nextcloud.com/endtoend/

### Espace de stockage supplémentaire
Vous manquez d'espace sur votre cloud ou votre courriel ? Enfin, nous avons ce qu'il vous faut. Dès à présent, vous pouvez demander de l'espace supplémentaire pour le cloud ou la boîte de courriel. Comme nous l'avons communiqué dans notre annonce précédente, le prix par Go est de 0,15 euro/mois et nous allons d'abord offrir 14 Go, 29 Go, 54 Go (espace total) d'expansion. Vous pouvez soumettre vos demandes via ce [formulaire](https://disroot.org/en/forms/extra-storage-space).

### Finances
Nous avons mis à jour notre tableau financier. Ça s'annonce bien ! Merci beaucoup à tous ceux qui ont contribué jusqu'à présent ! Nous étions très enthousiastes de voir que, pour la première fois, nous étions en mesure de payer les factures mensuelles de Disroot à partir des dons reçus. Nous espérons que nous continuerons à être surpris dans les mois à venir - alors n'arrêtez pas encore de "nous acheter du café". En outre, si nous avons besoin d'étendre ou de mettre à niveau le matériel, d'obtenir plus de serveurs (pour pouvoir héberger plus d'utilisateurs et plus de services), nous aurons besoin de fonds plus importants. Rappelez-vous qu'il n'y a pas de service "gratuit" et que le mot "cloud" n'est qu'un mot joli pour désigner l'ordinateur de quelqu'un d'autre.

### Nouveaux services ?
Nous recevons beaucoup de demandes de nouveaux services. Nous pensons également à des ajouts nous-mêmes. Naturellement, nous ne pouvons pas tous les fournir tout de suite en raison des ressources matérielles nécessaires ainsi que des heures de travail consacrées à leur maintenance. Étant donné les ressources limitées, nous avons besoin de décider quel est le service le plus nécessaire/attendu que vous aimeriez voir sur Disroot. Nous avons fait une courte liste de services que nous envisageons, jetez un coup d'oeil à ce [sondage](https://poll.disroot.org/ff3Qbrs1Kh74pHtN) et votez pour votre favori (aucune garantie cependant). Le scrutin se terminera le 15 avril.

<br><br>

**Au cours du mois à venir, nous allons nous calmer un peu et nous concentrer sur les petits travaux d'entretien et le nettoyage général du printemps.**

**Nous aimerions souhaiter à tout le monde de grandes célébrations printanières, et pour ceux d'entre vous dans l'hémisphère sud - des nuits d'automne agréables.**
