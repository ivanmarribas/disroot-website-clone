---
title: 'DisNews #2 - Rapporto annuale; project board su Gitea; lacre.io; Ansible e Gitea;'
date: '27-05-2021 0:10'
media_order: israestine.jpg
taxonomy:
  category: news
  tag: [disroot, news, yearly_report, report, gitea, lacre, ansible]
body_classes: 'single single-post'

---

Sono passati due mesi dalla nostra ultima newsletter. Come sempre ci sono tantissime cose da condividere con tutti voi. 

## Rapporto annuale
Un ritardo, quello della pubblicazione del rapporto annuale, che avremmo voluto evitare, ma ormai sembra una consolidata tradizione. Gennaio sembra ieri, mentre invece siamo a fine maggio. Quindi, anche se con un po' di ritardo, ecco il nostro rapporto del 2020. Per il prossimo anno ci siamo ripromessi maggior puntualità. Comunque meglio tardi che mai. Il rapporto di questo folle anno è disponibile [qui](https://disroot.org/en/annual_reports/AnnualReport2020.pdf).


## Disroot - Project board trasferito su Gitea

Negli ultimi mesi abbiamo utilizzato sempre di più la nostra istanza git al punto che ci siamo resi conto che era utile spostare nello stesso luogo tutti i documenti di coordinamento. E così, abbiamo deciso di spostare pure il Project-Board principale da Deck a Gitea. Attualmente quindi coordiniamo tutto il lavoro tramite Gitea. Troviamo che questa scelta incrementi l'apertura verso chi vuole affacciarsi a Disroot e maggior trasparenza sui progetti e il loro stato di avanzamento.

[Disroot Project](https://git.disroot.org/Disroot/Disroot-Project) è il nostro repository generale dove è possibile segnalare i problemi riscontrati, feedback, richieste di funzionalità ecc. È il punto centrale di coordinamento e di segnalazione di eventuali malfunzionamenti. 

[Changelog](https://git.disroot.org/Disroot/CHANGELOG) è pensato come repository per l'uso interno in termini di creazione e coordinamento dei lavori per le finestre di manutenzione bisettimanali, nonché un modo per tutti per vedere cosa c'è attualmente sul piatto. Tutti i changelog sono inoltre visibili pure al [seguente indirizzo](https://disroot.org/en/changelog).

## Ansible roles

Dall'inizio dell'anno stiamo lavorando intensamente per finire e pubblicare i nostri Ansible roles. È stato un lavoro faticoso ma ora, finalmente, iniziamo a vederne i frutti. Questo lavoro non solo è utile per aiutare gli altri per configurare facilmente i vari servizi, ma ha anche aggiunto enormi miglioramenti per quanto riguarda la stabilità della nostra piattaforma. Risulta inoltre utile per testare gli aggiornamenti e coordinare il lavoro internamente. Puoi trovare il repository [qui](https://git.disroot.org/Disroot-Ansible).

## Lacre

Abbiamo iniziato il progetto per crittografare le caselle di posta elettronica. Abbiamo deciso di rinominare il progetto da gpg mailgate a "Lacre". Lacre sta per "sigillo" in portoghese e spagnolo. Ci piace molto il nuovo nome e le possibili idee per i loghi o disegni che ne potrebbero scaturire. Attualmente stiamo iniziando a preparare l'ambiente di test. [https://lacre.io](https://lacre.io).

Legato a questo progetto, vogliamo ringraziare **pfm** che si è offerto come volontario per aiutare con la parte di sviluppo di Lacre. Lo ringraziamo e non vediamo l'ora di veder implementata la crittografia delle caselle di posta su Disroot. Siamo solo all'inizio, ma siamo molto felici ed entusiasti di avere dato il là al progetto. 

E per finire, a tutti i compagni che stanno lottando contro pandemia e conflitti devastanti in tutto il mondo: restate forti e non mollate!
