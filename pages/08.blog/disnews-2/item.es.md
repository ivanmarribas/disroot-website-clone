---
title: 'DisNews #2 - Reporte anual; Mudanza a Gitea; lacre.io; Ansible;'
date: '27-05-2021 0:10'
media_order: israestine.jpg
taxonomy:
  category: news
  tag: [disroot, novedades, reporte_anual, gitea, lacre, ansible]
body_classes: 'single single-post'

---

Dos meses han pasado desde nuestro último boletín. Como siempre, un montón de cosas para compartir con ustedes.

## Reporte Anual

Algo que queríamos evitar este año, pero que es como una nueva tradición... Parece que enero fue la semana pasada, aunque ya estamos a finales de mayo. Y aquí, nuestro reporte del año pasado (publicado a mediados de este año). Más vale tarde que nunca dicen algunos. Y sin embargo, esperamos hacer un mejor trabajo a partir de ahora y publicar estos informes anuales en enero. A pesar de todo, aquí está, un breve resumen de la locura que ha sido el año 2020 para todxs. ¡Que lo disfruten! E intentemos olvidar ese año lo antes posible. El reporte puede leerse [aquí](https://disroot.org/en/annual_reports/AnnualReport2020.pdf).


## Tablero de proyectos de Disroot. Moviendo todo a Gitea

Durante los últimos meses hemos estado utilizando nuestra instancia de git cada vez más. Hasta el punto en que nos dimos cuenta de que deberíamos mover completamente la coordinación de nuestro trabajo allí y tener todo en un solo lugar, lo cual tiene mucho más sentido para nosotros internamente. Así que también hemos decidido trasladar el tablero de proyecto principal de Deck a Gitea, desde donde ya estamos coordinando muchas cosas. Esto también nos dará a todxs un mejor panorama y más transparencia y apertura, dos de los temas en los que queríamos enfocarnos este año.

El tablero del [Proyecto Disroot](https://git.disroot.org/disroot/Disroot-Project) funciona como una pizarra general en la que nosotros, y lxs Disrooters, presentamos problemas, comentarios, discusiones, solicitudes de mejoras, etc, para luego distribuirlos entre los proyectos que alojamos (el sitio web, los temas, los roles de ansible, el registro de cambios, etc). Es un punto central de coordinación para nosotros y el lugar para que ustedes envíen sus comentarios y sugerencias. Nuestro repositorio de [Registro de cambios](https://git.disroot.org/Disroot/CHANGELOG) es para crear y coordinar de modo interno las tareas que conforman los trabajos de mantenimiento quincenales, y también una manera de ver en qué andamos. Todas las actualizaciones de este Registro ahora se publican en nuestro [sitio web](https://disroot.org/en/changelog) para que puedan también seguir los cambios anteriores en la plataforma.


## Roles de Ansible

Desde principios de 2021 hemos estado trabajando para terminar los roles de Ansible y publicarlos. Ha sido un trabajo infernal durante el último año, más o menos, y ahora finalmente estamos avanzando. Ver los resultados de todo el trabajo realizado hasta ahora es muy satisfactorio. Esto no solo puede ayudar a otras personas a configurar las cosas con facilidad, sino que también ha añadido enormes mejoras en cuanto a la estabilidad de la plataforma. También ayuda mucho para probar las actualizaciones antes de cualquier trabajo en producción, preparar nuevas características y cambios antes de que lleguen a los servidores y es un mejor modo de coordinar el trabajo entre nosotros internamente. Puedes encontrar el repositorio [aquí](https://git.disroot.org/Disroot-Ansible).


## Lacre

Estamos comenzando a trabajar en el cifrado del buzón. Hemos decidido cambiar el nombre del proyecto de "gpg mailgate" a "Lacre". Lacre, en portugués y español, es el nombre de un tipo de pasta sólida que, derretida, se utiliza para sellar cartas. Nos gustan mucho el nuevo nombre y las ideas para los logos/arte que surgen de él. Estamos comenzando lentamente, evaluando el trabajo y preparando el entorno de pruebas. El sitio del proyecto es: [https://lacre.io](https://lacre.io)

Queremos enviar nuestro reconocimiento a **@pfm** que se ofreció para ayudar con la parte de desarrollo del proyecto. Estamos muy agradecidos por la oferta y también ansiosos por lograr que el cifrado del buzón aterrice en Disroot. Todavía nos queda mucho camino por delante, pero estamos muy contentos y emocionados de haber empezado a avanzar.

Les deseamos a todxs un gran mes de junio, pero recuerden mantener las distancias todavía y cuidarse ustedes y a lxs demás. A todas nuestras amigas y amigos que están luchando contra la pandemia y los conflictos devastadores alrededor del mundo, les decimos: manténganse fuertes, cuídense.
