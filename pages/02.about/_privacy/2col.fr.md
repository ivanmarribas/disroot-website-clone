---
title: 'vie privée'
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

# Vie privée

Les logiciels peuvent être créés et transformés en n'importe quoi. Tous les boutons, toutes les couleurs et tous les liens que nous voyons et utilisons sur le web y ont été placés par quelqu'un. Lorsque nous utilisons les applications qui nous sont fournies, nous ne voyons généralement pas - et parfois nous nous en fichons - ce qui se passe derrière l'interface que nous utilisons. Nous communiquons avec d'autres personnes, nous stockons nos dossiers, organisons des réunions et des festivals, envoyons des courriels ou discutons pendant des heures et tout cela se passe comme par magie.
Au cours des dernières décennies, l'information est devenue très précieuse et de plus en plus facile à recueillir et à traiter. Nous avons l'habitude d'être analysés, acceptant aveuglément les termes et conditions pour "notre propre bien", faisant confiance aux autorités et à des entreprises de plusieurs milliards de dollars pour protéger nos intérêts, alors que nous sommes toujours le produit dans leurs "fermes de gens".

**Possédez vos propres données:**
De nombreux réseaux utilisent vos données pour gagner de l'argent en analysant vos interactions et en utilisant cette information pour vous fournir de la publicités. Disroot n'utilise pas vos données à d'autres fins que celles de vous permettre de vous connecter et d'utiliser le service.
Vos fichiers sur le cloud sont chiffrés avec votre mot de passe utilisateur, chaque bin-paste et fichier uploadé sur le service Lufi est également chiffré côté client, ce qui signifie que même les administrateurs du serveur n'ont pas accès à vos données. Chaque fois qu'il y a une possibilité de chiffrement, nous l'activons et si ce n'est pas possible, nous conseillons d'utiliser un logiciel de chiffrement externe. En tant qu'admins, moins nous connaissons vos données, mieux c'est :D. (_**Conseil du jour** : Puisque vos fichiers sur **Nextcloud** sont chiffrés avec votre mot de passe d'utilisateur, si vous l'oubliez, vous ne pourrez plus accéder à vos fichiers, donc **assurez-vous de ne JAMAIS perdre votre mot de passe** (nous suggérons d'utiliser un gestionnaire de mots de passe)_). [Lire ce tutoriel](https://howto.disroot.org/fr/tutorials/user/account/administration/ussc)

---

<br>
![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
