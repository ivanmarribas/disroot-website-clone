---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Leggi la nostra dichiarazione d'intenti</a>

---

**Disroot** è un progetto con sede ad Amsterdam, mantenuto da volontari e che dipende dal supporto della sua comunità.

Abbiamo originariamente creato **Disroot** per rispondere ai bisogni personali. Stavamo infatti cercando un software che ci permettesse di comunicare, condividere ed organizzarci al nostro interno.
La maggior parte delle soluzioni presenti avevano lacune per noi molto importanti: i nostri strumenti devono infatti essere **aperti**, **decentralizzati**, **federati** e rispettosi della **privacy** dell'utente.

Durante la ricerca di questi strumenti, abbiamo trovato dei progetti molto interessanti che credavamo dovessero essere accessibili a chiunque condividesse i nostri principi.
Abbiamo quindi deciso di unire e condividere alcune di queste applicazioni.

Così è come **Disroot** ha visto la sua nascita.

Grazie al progetto **Disroot** speriamo di cambiare il modo con coi le persone interagiscono sul Web. Vogliamo incoraggiare le persone ad utilizzare strumenti aperti e alternativi, sia sulla piattaforma Disroot sia su altre simili.

Assieme possiamo creare una rete indipendente che mette al centro le persone.
