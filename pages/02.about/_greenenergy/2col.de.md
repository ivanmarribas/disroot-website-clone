---
title: Online-donation
bgcolor: '#1F5C60'
wider_column: left
fontcolor: '#ffffff'
---

Die Disroot-Server werden in einem Rechenzentrum gehostet, das laut Betreiber ausschließlich erneuerbare Energien nutzt.

---

<a href="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" target=_blank><img src="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" alt="Diese Website wird umweltfreundlich gehostet - geprüft von thegreenwebfoundation.org" style="height:100px"></a>
