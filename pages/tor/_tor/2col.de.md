---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Download Tor Browser</a>

---

![](Tor_Logo.png)


## Die Website von Disroot über Tor erreichen

Du kannst die **Disroot-Website** über Tor mit dem Tor-Browser erreichen. Tor verhindert, dass jemand, der deine Verbindung beobachtet, herausfinden kann, dass du die Website von Disroot besuchst. 

Kopiere einfach die folgende .onion-Adresse und füge sie in den Tor-Browser ein: **j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion** oder wenn du bereits den Tor-Browser verwendest, klicke [hier](http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)

Projekt-Homepage: [https://www.torproject.org/](https://www.torproject.org/)
