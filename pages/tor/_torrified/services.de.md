---
title: Tor
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Webmail
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Freie und sichere E&#8209;Mail-Accounts, nutzbar mit einem IMAP-Client oder via Online-Benutzeroberfläche."
        button: 'http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion'
        buttontext: "Anmelden"
    -
        title: Pads
        icon: pads.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/pads'
        text: "Gemeinschaftlich und in Echtzeit Dokumente direkt im Webbrowser bearbeiten"
        button: 'http://b6mttjczryfoyz2go65hyjl5k6xfqqacpvz3ameurvraijxg5sv2z2id.onion'
        buttontext: "Ein Pad starten"
    -
        title: 'Paste-Bin'
        icon: pastebin.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/privatebin'
        text: "Minimalistischer, quelloffener und verschlüsselter Wegwerf-Container (Paste-Bin) mit Diskussionsboard."
        button: 'http://n63ite5off46lfh7qei4uhkvttrgvpve7ag3kwftlqkxo4o5mu7l4cqd.onion'
        buttontext: "Teile einen Paste-Bin"
    -
        title: Upload
        icon: upload.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/upload'
        text: "Software zur verschlüsselten, temporären Dateiaufbewahrung und -verbreitung."
        button: 'http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion'
        buttontext: "Datei teilen"
    -
        title: Suche
        icon: search.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/search'
        text: "Eine anonyme Meta-Suchmaschine."
        button: 'http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion'
        buttontext: "Suche"
    -
        title: 'Calls'
        icon: calls.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/calls'
        text: "Ein Werkzeug für Videokonferenzen."
        button: 'http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion'
        buttontext: "Aufruf"
    -
        title: 'Git'
        icon: git.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/git'
        text: "Eine Code-Hosting- und -Projektzusammenarbeits-Plattform."
        button: 'http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion'
        buttontext: "Anmelden"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/audio'
        text: "Eine Voice-Chat-Anwendung mit niedriger Latenz und hoher Sprachqualität."
        button: 'http://vd2k6x3cvqmm7pt2m76jpcyq7girnd3owykllkpu73rxzqv2cbs5diad.onion'
        buttontext: "Anmelden"
 
---
