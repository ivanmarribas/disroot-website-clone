---
title: Tor
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Webmail
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Comptes de messagerie gratuits et sécurisés pour votre client IMAP de bureau ou via une interface Web."
        button: 'http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion'
        buttontext: "Se connecter"
    -
        title: Pads
        icon: pads.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/pads'
        text: "Créez et modifiez des documents en collaboration et en temps réel."
        button: 'http://b6mttjczryfoyz2go65hyjl5k6xfqqacpvz3ameurvraijxg5sv2z2id.onion'
        buttontext: "Commencer"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/privatebin'
        text: "Paste-bin en ligne chiffré / forum de discussion."
        button: 'http://n63ite5off46lfh7qei4uhkvttrgvpve7ag3kwftlqkxo4o5mu7l4cqd.onion'
        buttontext: "Partager"
    -
        title: Upload
        icon: upload.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/upload'
        text: "Logiciel d'hébergement et de partage de fichiers temporaires et chiffrés."
        button: 'http://e2olmnzdp5d72z3xs2ugftvwgxywgbgipofa443zizolbgxoj5m46vyd.onion'
        buttontext: "Partager"
    -
        title: Search
        icon: search.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/search'
        text: "Une plate-forme anonyme multi moteurs de recherche."
        button: 'http://bzg6fq2cbzrp52z5xkmggsiqhfc4zb4ouq3g7y6b2yfdnuud6yajpyqd.onion'
        buttontext: "Rechercher"
    -
        title: 'Calls'
        icon: calls.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/calls'
        text: "Un outil de vidéo conférence."
        button: 'http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion'
        buttontext: "Appeler"
    -
        title: 'Git'
        icon: git.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/git'
        text: "Un hébergement de code et une collaboration de projet."
        button: 'http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion'
        buttontext: "Se connecter"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/audio'
        text: "Une application de chat vocal à faible latence et de haute qualité."
        button: 'http://vd2k6x3cvqmm7pt2m76jpcyq7girnd3owykllkpu73rxzqv2cbs5diad.onion'
        buttontext: "Créer une chaîne"

---
