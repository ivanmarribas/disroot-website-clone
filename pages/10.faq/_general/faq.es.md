---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "GENERALES"
section_number: "300"
faq:
    -
        question: "¿Qué significa Disroot?"
        answer: "<p><b>Des'raizar'</b>: Arrancar desde o por las raíces; por consiguiente, arrancar desde los cimientos; desarraigar.</p>"
    -
        question: "¿Disroot durará?"
        answer: "<p>Los administradores y mantenedores de <b>Disroot</b> usan la plataforma a diario y dependen de ella como su principal herramienta de comunicación. Pretendemos mantenerla andando por mucho tiempo.</p>"
    -
        question: "¿Cuánta gente utiliza Disroot?"
        answer: "<p>No mantenemos registros de lxs usuarixs activos así que no podemos responder esta pregunta. Además, consideramos que no es, de ninguna manera, una forma de medir la salud de la plataforma. Las cantidades de usuarixs son susceptibles de ser manipuladas por muchas razones, por ejemplo, para llamar la atención y para satisfacer inversionistas, entre otras. Como los objetivos de la plataforma no tienen nada que ver con ninguno de esos, entendemos que no tiene ningún sentido dar estadísticas falsas o manipuladas al respecto.</p>"

---
