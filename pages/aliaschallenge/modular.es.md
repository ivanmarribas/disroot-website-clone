---
title: Desafío del Alias
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _intro
            - _goal
            - _domains
            - _rewardstitle
            - _rewards
            - _ranking
            - _pagebottom
body_classes: modular
header_image: 'header2.jpg'
---
