---
title: 'Déclaration de mission'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
---

---

### Les aspects pratiques

Afin de faire fonctionner le projet Disroot.org selon les principes ci-dessus, nous travaillons selon les principes pratiques suivants :

##### 1) Financement

Disroot.org est une fondation à but non lucratif et tous les revenus de la plateforme Disroot sont réinvestis dans le projet, la maintenance des services et le développement de logiciels libres.

Nous reconnaissons différentes façons de financer le projet et son développement.

A. Dons des utilisateurs.
<br>
Nous pensons que mettre un prix sur nos services, c'est, d'une certaine façon, limiter l'autonomie des utilisateurs. Nous confions donc cette responsabilité aux Disrooters. Nous comprenons que la valeur de l'argent varie en fonction de différents facteurs, tels que la situation géographique, la situation socio-économique ou la situation financière personnelle, nous avons donc décidé de laisser les Disrooters évaluer eux-mêmes à quelle hauteur ils peuvent contribuer à ce projet.
<br>
Il est important de souligner que rien sur Internet n'est gratuit et que les services en ligne ont toujours un coût qui est payé avec de l'argent réel ou des données privées précieuses. Nous sommes convaincus que la liberté de l'Internet ne peut être atteinte et maintenue que si les gens s'en rendent compte et y contribuent volontairement. Comme Disroot.org ne vend pas les données des utilisateurs, nous comptons beaucoup sur les dons des utilisateurs.

B. Subventions ou bourses.
<br>
Les subventions et autres formes de financement ne seront acceptées que si elles ne nuisent en aucune façon à l'indépendance du projet. Nous ne mettrons pas en péril la vie privée des Disrooters, notre liberté et la façon dont nous voulons gérer le service, pour répondre aux exigences fixées par les "donateurs". Nous sommes conscients que les subventions sont souvent allouées dans un but ou un résultat spécifique, mais nous sommes également convaincus que certaines de ces exigences peuvent être satisfaites sans nuire au projet ou au réseau fédéré indépendant dans son ensemble. Nous examinerons attentivement toutes les subventions possibles avant de les accepter afin de nous assurer qu'elles ne contredisent pas les principaux principes du projet.

##### 2) Processus de prise de décision
Nous suivons un processus décisionnel basé sur le consensus. Au sein de l'équipe centrale de Disroot, tout le monde a une voix et une opinion égales. Toutes les décisions importantes sont prises au cours de réunions au cours desquelles les propositions sont présentées et discutées en détail afin que chacun ait le temps d'élaborer ses opinions ou préoccupations avant de parvenir à un accord mutuel.

##### 3) Inclusion communautaire
Disroot.org n'aurait aucun sens sans la communauté dynamique qui l'entoure, dont la participation active au projet est très importante et encouragée. Nous sommes ouverts à toute aide, amélioration, rétroaction et suggestions et nous offrons divers moyens de communication entre les membres de la communauté Disroot et l'équipe centrale, tels que : forums, divers tableaux de projet de Taiga ou des salles de messagerie instantanée.
