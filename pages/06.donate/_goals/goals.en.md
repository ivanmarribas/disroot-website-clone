---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Share our Good fortune'
        text: "If we receive at least 400 EUR in donations, we share 15% of our surplus with the developers of the software we are using. Disroot would not exist without those developers."
        unlock: yes
    -
        title: 'Pay one Volunteer Fee'
        text: "If we have more than 150€ left at the end of the month, we pay to one Disroot Core member a volunteer fee of 150€."
        unlock: yes
    -
        title: 'Pay two Volunteer Fees'
        text: "If we have more than 300€ left at the end of the month, we pay to two Disroot Core members a volunteer fee of 150€."
        unlock: yes
    -
        title: 'Pay three Volunteer Fees'
        text: "If we have more than 450€ left at the end of the month, we pay to three Disroot Core members a volunteer fee of 150€."
        unlock: yes
    -
        title: 'Pay four Volunteer Fees'
        text: "If we have more than 600€ left at the end of the month, we pay to four Disroot Core members a volunteer fee of 150€."
        unlock: no
    -
        title: 'Pay five Volunteer Fees'
        text: "If we have more than 750€ left at the end of the month, we pay to five Disroot Core members a volunteer fee of 150€."
        unlock: no
---

<div class=goals markdown=1>

</div>
