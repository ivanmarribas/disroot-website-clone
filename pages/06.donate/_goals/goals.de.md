---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Wir teilen die Spenden'
        text: "Wenn wir mindestens 400€ an Spenden erhalten, teilen wir 15 % unseres Überschusses mit den Entwicklern der Software, die wir nutzen. Disroot würde ohne diese Entwickler nicht existieren."
        unlock: yes
    -
        title: 'Eine Aufwands&shy;entschädigung für einen Helfer'
        text: "Wenn wir am Ende des Monats mehr als 150€ übrig haben, zahlen wir einem Mitglied des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: yes
    -
        title: 'Eine Aufwands&shy;entschädigung für zwei Helfer'
        text: "Wenn wir am Ende des Monats mehr als 300€ übrig haben, zahlen wir zwei Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: yes
    -
        title: 'Eine Aufwands&shy;entschädigung für drei Helfer'
        text: "Wenn wir am Ende des Monats mehr als 450€ übrig haben, zahlen wir drei Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: yes
    -
        title: 'Eine Aufwands&shy;entschädigung für vier Helfer'
        text: "Wenn wir am Ende des Monats mehr als 600€ übrig haben, zahlen wir vier Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: no
    -
        title: 'Eine Aufwands&shy;entschädigung für fünf Helfer'
        text: "Wenn wir am Ende des Monats mehr als 600€ übrig haben, zahlen wir fünf Mitgliedern des Disroot-Teams eine Aufwandsentschädigung von 150€."
        unlock: no
---

<div class=goals markdown=1>

</div>
