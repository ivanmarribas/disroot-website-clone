---
title: 'Etape 4'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
#bgcolor: '#C6FF9D'
#fontcolor: '#555'
wider_column: left
---

# Etape 4:
## Demander un lien vers un domaine personnalisé


---
<br>
<br>

Nous y sommes presque...
Si vous avez obtenu votre domaine, configuré les DNS et injecté de la caféine fraîche à vos admins, vous pouvez maintenant remplir le formulaire et soumettre votre demande de lien vers un domaine personnalisé.

Nous traitons les demandes chaque vendredi après-midi. Une fois traitées, vous recevrez un e-mail contenant toutes les informations nécessaires pour vous permettre d'envoyer des messages depuis votre propre domaine.
<br>
<a class="button button1" href="forms/domain-linking-form">Demande de lien vers un domaine</a>
