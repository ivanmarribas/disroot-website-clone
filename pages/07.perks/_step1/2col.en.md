---
title: 'Step 1'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Step 1:
## Get a domain



---      

<br>
If you don't have one yet, you need to get it first. At this moment **Disroot** does not provide domain purchase feature. Although there is plenty of domain providers around, so you can do some search and go shopping. We suggest you try to choose some more local, ethical, small domain registrar instead of going to big business ones.
Support your local economy.
